<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@page import="java.time.LocalDate"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
</head>
<body>
	<%
	int i = 0;
	Utilisateur user = (Utilisateur) request.getSession().getAttribute("utilisateur");
	String deconnecter = (String) request.getAttribute("deconnecter");
	String connecter = (String) request.getAttribute("connecter");
	String AchatsVentes = (String) request.getAttribute("AchatsVentes");
	String enchereOuverte = (String) request.getAttribute("enchereOuverte");
	String mesEnchere = (String) request.getAttribute("mesEnchere");
	String mesEnchereRemportees = (String) request.getAttribute("mesEnchereRemportees");
	String mesVentesEnCours = (String) request.getAttribute("mesVentesEnCours");
	String ventesNonDebutees = (String) request.getAttribute("ventesNonDebutees");
	String ventesTerminees = (String) request.getAttribute("ventesTerminees");
	String disabled;
	LocalDate dateNow = LocalDate.now();
	%>
	<header class="global-nav">
		<div>
			<a class="link-primary liens" href="Accueil">ENI-Enchères</a>
			<%
			if (user != null) {
			%>
			<a class="link-primary liens"
				href="<%=request.getContextPath()%>/Identification" <%=deconnecter%>>S'inscrire
				- Se connecter</a> <a class="link-primary liens"
				href="<%=request.getContextPath()%>/Accueil" <%=connecter%>>Enchère</a>
			<label <%=connecter%>>&nbsp;</label> <a class="link-primary liens"
				href="<%=request.getContextPath()%>/CreateArticle" <%=connecter%>>Vendre
				un article</a> <label <%=connecter%>>&nbsp;</label> <a
				class="link-primary liens"
				href="<%=request.getContextPath()%>/DisplayUser" <%=connecter%>>Mon
				Profil</a> <label <%=connecter%>>&nbsp;</label>

			<form class="liens" method="POST" action="Accueil">
				<input id="exclure_bouton" class="link-primary bouton boutonLien"
					type="submit" name="deconnexion" value="deconnexion" <%=connecter%> />
			</form>
			<%
			} else {
			%>
			<a class="link-primary"
				href="<%=request.getContextPath()%>/Identification"
				class="textAlignR" <%=deconnecter%>>S'inscrire - Se connecter</a> <a
				class="link-primary"
				href="<%=request.getContextPath()%>/DisplayUser" class="textAlignR"
				<%=connecter%>>Enchère</a>
			<%
			}
			%>
		</div>
	</header>
	<br>
	<h2 class="text-center">Liste des Encheres</h2>
	<br>
	<form method="POST" action="Accueil">
		<section class="formulaire container-fluid col-6">
			<div class="col-12-sm">
				<label>Filtres :</label>
				<input class="col-9-sm" id="input_filtre" type="text" name="filtre" />
			</div>
			<div class="col-12-sm">
				<label for="categorie">Categorie : </label>
				<select class="col-9-sm" name="categorie" id="cat">
					<option value="toute">Toutes</option>
					<option value="informatique">Informatique</option>
					<option value="ameublement">Ameublement</option>
					<option value="vetement">Vêtement</option>
					<option value="sportEtLoisir">Sport et Loisir</option>
				</select>
			</div>
			<div>
				<input type="submit" value="Rechercher" />
			</div>
		</section>
		<section class="formulaire container col-6 <%=connecter%>">
			<div class="container col-4 block">
				<div>
					<%
					if (AchatsVentes == null || AchatsVentes.equals("Achats")) {
					%>
					<%
					disabled = "";
					%>
					<input type="radio" id="Achats" name="AchatsVentes" value="Achats"
						onchange="submit()" checked>
					<%
					} else {
					%>
					<%
					disabled = "disabled ";
					%>
					<input type="radio" id="Achats" name="AchatsVentes" value="Achats"
						onchange="submit()">
					<%
					}
					%>
					<label for="Achats">Achats</label>
				</div>
				<div>
				<input type="checkbox" name="enchereOuverte" onchange="submit()" <%=disabled%> <%=enchereOuverte%>>
				<label>&#160&#160&#160&#160Enchère ouverte</label>
				</div>
				<div>
					<input type="checkbox" name="mesEnchere" onchange="submit()" <%=disabled%> <%=mesEnchere%>>
					<label>&#160&#160&#160&#160Mes enchère</label>
				</div>
				<div>
					<input type="checkbox" name="mesEnchereRemportees" onchange="submit()" <%=disabled%> <%=mesEnchereRemportees%>>
					<label>&#160&#160&#160&#160Mes enchère remportées</label>
				</div>
			</div>
			<div class="container col-4 block">
				<div>
					<%
					if (AchatsVentes != null && AchatsVentes.equals("Ventes")) {
					%>
					<%
					disabled = "";
					%>
					<input type="radio" id="Ventes" name="AchatsVentes" value="Ventes"
						onchange="submit()" checked>
					<%
					} else {
					%>
					<%
					disabled = "disabled ";
					%>
					<input type="radio" id="Ventes" name="AchatsVentes" value="Ventes"
						onchange="submit()">
					<%
					}
					%>
					<label for="Ventes">Mes ventes</label>
				</div>
				<div>
					<input type="checkbox" name="mesVentesEnCours" onchange="submit()" <%=disabled%> <%=mesVentesEnCours%>>
					<label>&#160&#160&#160&#160Mes ventes en cours</label>
				</div>
				<div>
					</label> <input type="checkbox" name="ventesNonDebutees" onchange="submit()" <%=disabled%> <%=ventesNonDebutees%>>
						<label>&#160&#160&#160&#160Ventes non débutées</label>
				</div>
				<div>
					</label> <input type="checkbox" name="ventesTerminees" onchange="submit()" <%=disabled%> <%=ventesTerminees%>>
					<label>&#160&#160&#160&#160Ventes terminées</label>
				</div>
			</div>
		</section>
		<br><br>
	</form>
		<c:if test="${empty ListeArticle}">
			<p>Pas d'article correspondant a votre recherche</p>
		</c:if>
		<c:if test="${!empty ListeArticle}">
			<div class="container col-12 ">
				<c:forEach var="Article" items="${ListeArticle}">
					<div class="container col-3 block border border-secondary rounded">
						<%
						if (("Ventes").equals(AchatsVentes)) {
						%>
						<c:if test="${Article.before()== true}">
							<form method="post" action="ModifieArticle">
								<button type="submit" style="text-decoration: underline;" value="${Article.getId()}" name="idArticle">${Article.getNom()}</button>
							</form>
						</c:if>
						<c:if test="${Article.before()== false}">
							<%
							if (user != null) {
							%>
							<form method="post" action="DisplayBid">
								<button type="submit" value="${Article.getId()}" name="idArticle" >${Article.getNom()}</button>
							</form>
							<%
							} else {
							%>
							<form method="post" action="DisplayBid">
								<button type="submit" value="${Article.getId()}" name="idArticle">${Article.getNom()}</button>
							</form>
							<%
							}
							%>
						</c:if>

						<%
						} else {
						%>
						<form method="post" action="DisplayBid">
							<button type="submit" value="${Article.getId()}" name="idArticle">${Article.getNom()}</button>
						</form>
						<%
						}
						%>
						<br>
						<div>Prix : ${Article.getPrixVente()}</div>
						<div>Fin de l'enchere : ${Article.getDateFinEncheres()}</div>
						<br>
						<c:forEach var="Utilisateur" begin="<%=i%>" end="<%=i%>" items="${ListeUtilisateur}">
							<div class="col-12-sm">	
							<form method="get" action="DisplayUser">
								<label class="col-9-sm" style="width:70px;" for="categorie">Vendeur : </label>
								<button type="submit" value="${Utilisateur.getId()}" name="ID">${Utilisateur.getPseudo()}</button>
							</form>
							</div>
						</c:forEach>
						<%
						i += 1;
						%>
					</div>
				</c:forEach>
			</div>
		</c:if>
</body>
</html>