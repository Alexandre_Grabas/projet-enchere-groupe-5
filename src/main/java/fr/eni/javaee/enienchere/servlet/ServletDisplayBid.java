package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Categorie;
import fr.eni.javaee.enienchere.bo.Enchere;
import fr.eni.javaee.enienchere.bo.Retrait;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ServletDisplayBid
 */
@WebServlet("/DisplayBid")
public class ServletDisplayBid extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletDisplayBid() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/displayBid.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// Variables
		EnchereManager enchereManager = new EnchereManager();
		Integer idArticle = null;
		Article article = new Article();
		Integer idUtilisateur = null;
		Utilisateur utilisateur = null;
		Integer idVendeur;
		Utilisateur vendeur = new Utilisateur();
		Integer idAcheteur;
		Utilisateur acheteur = new Utilisateur();

		// Récupération de l'id de l'article spécifié (obligatoire)
		if (request.getParameter("idArticle") != null) {
			idArticle = Integer.valueOf(request.getParameter("idArticle"));
		} else {
			System.out.println("ServletDisplayBid -> Pas d'id d'article récupéré");
		}

		// Récupération de l'utilisateur dans la session (s'il y en a un)
		if (request.getSession().getAttribute("utilisateur") != null) {
			idUtilisateur = ((Utilisateur) request.getSession().getAttribute("utilisateur")).getId();
			utilisateur = enchereManager.selectUtilisateurById(idUtilisateur);
		} else {
			System.out.println("ServletDisplayBid -> Pas d'utilisateur récupéré");
		}

		if (idArticle != null && idArticle > 0) {

			// Récupération et envoi de l'article grace à l'id de l'article
			article = enchereManager.selectArticleById(idArticle);
			request.setAttribute("Article", article);

			// Récupération et envoi du vendeur grace à l'idUtilisateur de l'article
			vendeur = enchereManager.selectUtilisateurById(article.getIdUtilisateur());
			idVendeur = vendeur.getId();
			request.setAttribute("Vendeur", vendeur);

			// Récupération et envoi de l'acheteur grace à l'id de l'article
			idAcheteur = enchereManager.selectLastEnchereByIdArticle(idArticle).getIdAcheteur();
			if (idAcheteur != null && idAcheteur > 0) {
				acheteur = enchereManager.selectUtilisateurById(idAcheteur);
				request.setAttribute("Acheteur", acheteur);
			} else {
				System.out.println("ServletDisplayBid ->  Pas d'acheteur récupéré");
			}

			// Récupération en envoi de la catégorie grace à l'idCategorie de l'article
			Categorie categorie = enchereManager.selectCategorieById(article.getIdCategorie());
			request.setAttribute("Categorie", categorie);

			// Récupération et envoi du retrait grace à l'idRetrait de l'article
			Retrait retrait = enchereManager.selectRetraitById(article.getIdRetrait());
			request.setAttribute("Retrait", retrait);

			// Lors de l'appui sur le bouton "Enchérir, si le prix n'est pas "null" ...
			if (request.getParameter("prix") != null
					// ... ET que l'utilisateur a assez de crédit ...
					&& Integer.valueOf(request.getParameter("prix")) <= utilisateur.getCredit()) {
				try {
					// S'il un autre acheteur avait déjà fait une enchère sur l'article ...
					if (idAcheteur != null && idAcheteur > 0) {
						// Remboursement de cet acheteur
						Integer credit = acheteur.getCredit() + article.getPrixVente().intValue();
						enchereManager.updateCreditUtilisateurById(idAcheteur, credit);
					}

					// Modification du prix de l'article dans la table article
					Double montant = Double.valueOf(request.getParameter("prix"));
					enchereManager.updatePrixArticleById(idArticle, montant);

					// Suppression du nombre de crédits à l'utilisateur
					if (idUtilisateur != null && idUtilisateur > 0) {
						Integer credit = utilisateur.getCredit() - montant.intValue();
						enchereManager.updateCreditUtilisateurById(idUtilisateur, credit);
					}

					// Ajout d'une nouvelle enchère dans la table enchère
					Enchere enchere = new Enchere(LocalDate.now(), montant, idArticle, idUtilisateur);
					enchereManager.ajouterEnchere(enchere);

					// Récupération et renvoi des données du nouvel acheteur (= utilisateur)
					acheteur = enchereManager.selectUtilisateurById(idUtilisateur);
					request.setAttribute("Acheteur", acheteur);

					// Renvoi des données de l'article dont le prix a été modifié
					article = enchereManager.selectArticleById(idArticle);
					request.setAttribute("Article", article);

				} catch (EnchereException e) {
					System.out.println(e.getMessage());
				}
			}

			// Si l'acheteur appuie sur le bouton de retrait ...
			if (request.getParameter("retrait") != null) {
				// Vérification de la récupération d'un vendeur
				if (idVendeur != null && idVendeur > 0) {
					// Ajout du prix de l'article aux crédits du vendeur
					Integer credit = vendeur.getCredit() + article.getPrixVente().intValue();
					try {
						// Ajout du prix aux crédits du vendeur
						enchereManager.updateCreditUtilisateurById(idVendeur, credit);

						// Modification de l'etat de l'article à "récupéré"
						enchereManager.updateEtatArticleById(idArticle, 0);

						// // Renvoi des données de l'article dont l'etat a été modifié
						article = enchereManager.selectArticleById(idArticle);
						request.setAttribute("Article", article);
						System.out.println(article.getEtatVente());
					} catch (EnchereException e) {
						System.out.println(e.getMessage());
					}
				}

			}

		} else {
			// TODO : gestion d'erreur
			System.out.println("Pas d'article récupéré");
		}

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/displayBid.jsp");
		rd.forward(request, response);
	}

}
