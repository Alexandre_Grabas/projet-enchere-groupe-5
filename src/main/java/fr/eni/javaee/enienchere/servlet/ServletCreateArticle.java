package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Retrait;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ServletCreateArticle
 */
@WebServlet("/CreateArticle")
public class ServletCreateArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletCreateArticle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnchereManager enchere = new EnchereManager();
		
		request.setAttribute("articleObject",new Article(0, "", "", null, null, 100.0, null, null, null, null));
		request.setAttribute("categories",enchere.selectAllCategories());
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/createAndModifieArticle.jsp");
		rd.forward(request, response);
	}

	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer idArticle = Integer.parseInt(request.getParameter("idArticle"));
		String nom = request.getParameter("nom");
		String description = request.getParameter("description");
		String categorie = request.getParameter("categorie");
		String mise = request.getParameter("mise");
		String dateIn = request.getParameter("dateIn");
		String dateOut = request.getParameter("dateOut");
		String rue = request.getParameter("rue");
		String cp = request.getParameter("cp");
		String ville = request.getParameter("ville");
		Integer idRetrait = null;
		EnchereManager E = new EnchereManager();
		List<String> error_messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		Utilisateur user = (Utilisateur)session.getAttribute("utilisateur");
		String Regex = "";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		
		// On verifie que le retrait est pr�sent dans la base
		try {
			idRetrait = E.selectRetraitIfPresent(new Retrait(rue.toLowerCase(), cp, ville.toLowerCase()));
			System.out.println("resultat du select: "+idRetrait+" avec les infos suivantes: "+rue+" + "+cp+" + "+ville);
		} catch (EnchereException e) {
			e.printStackTrace();
		}
		
		// Controle des champs et renvoie d'erreur en fonction
		if (nom.isEmpty() || nom.isBlank()) {
			error_messages.add("Le champ article est vide ou ne contient que des espaces !");
		}
		if (description.isEmpty() || description.isBlank()) {
			error_messages.add("Le champ description est vide ou ne contient que des espaces !");
		}
		if (categorie.isEmpty() || categorie.isBlank()) {
			error_messages.add("Le champ categorie est vide ou ne contient que des espaces !");
		}
		if (mise.isEmpty() || mise.isBlank()) {
			error_messages.add("Le champ mise est vide ou ne contient que des espaces !");
		}
		if (dateIn.isEmpty() || dateIn.isBlank() || dateOut.isEmpty() || dateOut.isBlank()) {
			error_messages.add("Un des champs date de d�but/fin d'ench�re sont vide ou ne contiennent que des espaces !");
		} else if (LocalDate.parse(dateIn, formatter).isAfter(LocalDate.parse(dateOut, formatter))) {
			error_messages.add("La date de d�but d'ench�re est situ�e apr�s la date de fin d'ench�re !");
		} else if (LocalDate.parse(dateOut, formatter).isBefore(LocalDate.now())) {
			error_messages.add("La date de fin d'ench�re est d�j� d�pass�e !");
		}
		if (rue.isEmpty() || rue.isBlank()) {
			error_messages.add("Le champ rue est vide ou ne contient que des espaces !");
		}
		if (cp.isEmpty() || cp.isBlank()) {
			error_messages.add("Le champ code postal est vide ou ne contient que des espaces !");
		}
		Regex = "\\d{5}";
		if (!cp.matches(Regex)) {
			error_messages.add("Le format du code postal est invalide (5 chiffres).");
		}
		if (ville.isEmpty() || ville.isBlank()) {
			error_messages.add("Le champ ville est vide ou ne contient que des espaces !");
		}
		
		// on insere le nouvel idRetrait si aucune autre erreur et que l'id n'a pas �t� trouv�
		if (error_messages.isEmpty() & idRetrait == null) {
			try {
				idRetrait = E.ajouterRetrait(new Retrait(rue.toLowerCase(), cp, ville.toLowerCase()));
				System.out.println("Resultat de l'ajout d'un nouveau point de retrait: "+idRetrait+" avec les infos suivantes: "+rue+" + "+cp+" + "+ville);
			} catch (EnchereException e) {
				e.printStackTrace();
			}
		}
		
		if (error_messages.isEmpty() & idArticle == 0) {
			// On traite le cas ou c'est une cr�ation d'article 
						
			try {
				Article a = new Article(nom, description, LocalDate.parse(dateIn, formatter), LocalDate.parse(dateOut, formatter), Double.parseDouble(mise), Double.parseDouble(mise), user.getId(), Integer.parseInt(categorie), idRetrait);
				System.out.println("cr�ation de l'article avec les infos suivantes:");
				System.out.println(a);
				E.ajouterArticle(a);
			} catch (EnchereException e) {
				e.printStackTrace();
			}

			response.sendRedirect("Accueil");
		} else if (error_messages.isEmpty() & idArticle != 0){
			// On traite le cas ou c'est une modification d'article 
			
			Article a = new Article(idArticle, nom, description, LocalDate.parse(dateIn, formatter), LocalDate.parse(dateOut, formatter), Double.parseDouble(mise), Double.parseDouble(mise), user.getId(), Integer.parseInt(categorie), idRetrait);
			System.out.println("Modification de l'article avec les infos suivantes:");
			System.out.println(a);
			E.updateArticle(a);
			
			response.sendRedirect("Accueil");
		} else {
			EnchereManager enchere = new EnchereManager();
			
			// on renvoie le maximum de champs pour ne pas r��crire tout si une erreur de saisie
			if (idArticle == 0) {
				request.setAttribute("articleObject",new Article(0, nom, description, LocalDate.parse(dateIn, formatter), LocalDate.parse(dateOut, formatter), Double.parseDouble(mise), Double.parseDouble(mise), user.getId(), Integer.parseInt(categorie), idRetrait));
			} else {
				request.setAttribute("articleObject",new Article(idArticle, nom, description, LocalDate.parse(dateIn, formatter), LocalDate.parse(dateOut, formatter), Double.parseDouble(mise), Double.parseDouble(mise), user.getId(), Integer.parseInt(categorie), idRetrait));
				request.setAttribute("retrait",new Retrait(rue,cp,ville));
			}
			
			request.setAttribute("categories",enchere.selectAllCategories());
			request.setAttribute("error_messages", error_messages);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/createAndModifieArticle.jsp");
			rd.forward(request, response);
		}
		
	}

}
