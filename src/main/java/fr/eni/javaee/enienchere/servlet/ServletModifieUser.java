package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ChangeProfil
 */
@WebServlet("/ModifieUser")
public class ServletModifieUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletModifieUser() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/modifieUser.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pseudo = request.getParameter("pseudo");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String email = request.getParameter("email");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String ville = request.getParameter("ville");
		String cp = request.getParameter("cp");
		String pwd = request.getParameter("pwd");
		String new_pwd = request.getParameter("new_pwd");
		String confirm_pwd = request.getParameter("confirm_pwd");
		
		HttpSession session = request.getSession();
		Utilisateur user = (Utilisateur)session.getAttribute("utilisateur");
		
		// on empile des messages d'erreurs en fonctions de controles sp�cifiques
		List<String> error_messages = formControl(pseudo, nom, prenom, email, telephone, rue, ville, cp, new_pwd, confirm_pwd);
		
		// On renvoie vers la page avec un message d'erreur ou on update + r�affiche le profile
		if (error_messages.isEmpty()) {
			
			// on change le mot de passe si non vide et egalite sur le mot de passe
			if (!new_pwd.isEmpty() && !new_pwd.isBlank() && !confirm_pwd.isEmpty() && !confirm_pwd.isBlank()) {
				if (new_pwd.equals(confirm_pwd)) {
					pwd = confirm_pwd;
				}
			}
			
			// on recr��e l'utilisateur en fonction des nouvelles donn�es puis on update cet utilisateur
			try {
				Utilisateur u = new Utilisateur(user.getId(),pseudo,nom,prenom,email,telephone,rue,ville,cp,pwd,user.getCredit(),user.isAdministrateur());
				System.out.println("L'utilisateur en cours de session va �tre mis � jours avec ces informations: "+u);
				EnchereManager update = new EnchereManager();
				update.updateUtilisateurById(u);
				session.setAttribute("utilisateur", u); // on met � jours la session
			} catch (EnchereException e) {
				e.printStackTrace();
			}
			
			response.sendRedirect("DisplayUser");
		} else {
			request.setAttribute("error_messages", error_messages);
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/modifieUser.jsp");
			rd.forward(request, response);
		}
	}
	
	/**
	 * Methode qui permet de v�rifier les erreurs de saisies des champs d'un formulaire (voir @param)
	 * @param pseudo
	 * @param nom
	 * @param prenom
	 * @param email
	 * @param telephone
	 * @param rue
	 * @param ville
	 * @param cp
	 * @param new_pwd
	 * @param confirm_pwd
	 * @return une liste de textes d'erreurs List<String> en fonction des champs � v�rifier
	 */
	private List<String> formControl(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String new_pwd, String confirm_pwd) {
		List<String> error_messages = new ArrayList<String>();
		String Regex = "";
		
		// Controle des champs et renvoie d'erreur en fonction
		if (!new_pwd.isEmpty() && !confirm_pwd.isEmpty()) {
			if (!new_pwd.equals(confirm_pwd)) {
				error_messages.add("Le nouveau mot de passe et sa confirmation ne correspondent pas !");
			}
		}		
		if (pseudo.isEmpty() || pseudo.isBlank()) {
			error_messages.add("Le champ pseudo est vide ou ne contient que des espaces !");
		}
		if (nom.isEmpty() || nom.isBlank()) {
			error_messages.add("Le champ nom est vide ou ne contient que des espaces !");
		}
		if (prenom.isEmpty() || prenom.isBlank()) {
			error_messages.add("Le champ prenom est vide ou ne contient que des espaces !");
		}
		if (email.isEmpty() || email.isBlank()) {
			error_messages.add("Le champ email est vide ou ne contient que des espaces !");
		}
		Regex = "^[A-Za-z0-9_.-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}$";
		if (!email.matches(Regex)) {
			error_messages.add("Le format de l'email est invalide.");
		}
		if (telephone.isEmpty() || telephone.isBlank()) {
			error_messages.add("Le champ telephone est vide ou ne contient que des espaces !");
		}
		if (rue.isEmpty() || rue.isBlank()) {
			error_messages.add("Le champ rue est vide ou ne contient que des espaces !");
		}
		if (cp.isEmpty() || cp.isBlank()) {
			error_messages.add("Le champ cp est vide ou ne contient que des espaces !");
		}
		Regex = "\\d{5}";
		if (!cp.matches(Regex)) {
			error_messages.add("Le format du code postal est invalide (5 chiffres).");
		}
		if (ville.isEmpty() || ville.isBlank()) {
			error_messages.add("Le champ ville est vide ou ne contient que des espaces !");
		}

		System.out.println("+++ D�but des messages d'erreurs page modification user +++");
		for (String string : error_messages) {
			System.out.println(string);
		}
		System.out.println("+++ Fin des messages d'erreurs page modification user +++");
		return error_messages;
	}

}
