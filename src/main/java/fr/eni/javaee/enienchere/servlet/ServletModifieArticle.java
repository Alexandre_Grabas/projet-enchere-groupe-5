package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Retrait;

/**
 * Servlet implementation class ServletDisplayArticle
 */
@WebServlet("/ModifieArticle")
public class ServletModifieArticle extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletModifieArticle() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EnchereManager manager = new EnchereManager();

		String id = request.getParameter("idArticle");
		Article a = manager.selectArticleById(Integer.parseInt(id));
		Retrait r = manager.selectRetraitById(a.getIdRetrait());
		System.out.println("L'article choisie est celui-ci: "+a);
		
		if (a != null) {
			request.setAttribute("articleObject",a);
			request.setAttribute("retrait",r);
		}
//		else {
//			article inconnu: quelle page?
//		}
		
		request.setAttribute("categories",manager.selectAllCategories());
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/createAndModifieArticle.jsp");
		rd.forward(request, response);
	}

}
