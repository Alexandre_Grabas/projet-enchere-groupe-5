package fr.eni.javaee.enienchere.bo;

import java.io.Serializable;
import java.time.LocalDate;

public class Article implements Serializable {

	private static final long serialVersionUID = 1L;

	// Variables
	private Integer id;
	private String nom;
	private String description;
	private LocalDate dateDebutEncheres;
	private LocalDate dateFinEncheres;
	private Double miseAPrix;
	private Double prixVente;
	private Integer idUtilisateur;
	private Integer idCategorie;
	private Integer idRetrait;
	private Integer etatVente = 1;

	// Constructeurs
	public Article() {
	}

	public Article(String nom, String description, LocalDate dateDebutEncheres, LocalDate dateFinEncheres,
			Double miseAPrix, Double prixVente, Integer idUtilisateur, Integer idCategorie, Integer idRetrait) {
		super();
		this.nom = nom;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.miseAPrix = miseAPrix;
		this.prixVente = prixVente;
		this.idUtilisateur = idUtilisateur;
		this.idCategorie = idCategorie;
		this.idRetrait = idRetrait;
	}

	public Article(Integer id, String nom, String description, LocalDate dateDebutEncheres, LocalDate dateFinEncheres,
			Double miseAPrix, Double prixVente, Integer idUtilisateur, Integer idCategorie, Integer idRetrait) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.miseAPrix = miseAPrix;
		this.prixVente = prixVente;
		this.idUtilisateur = idUtilisateur;
		this.idCategorie = idCategorie;
		this.idRetrait = idRetrait;
	}

	public Article(Integer id, String nom, String description, LocalDate dateDebutEncheres, LocalDate dateFinEncheres,
			Double miseAPrix, Double prixVente, Integer idUtilisateur, Integer idCategorie, Integer idRetrait,
			Integer etatVente) {
		super();
		this.id = id;
		this.nom = nom;
		this.description = description;
		this.dateDebutEncheres = dateDebutEncheres;
		this.dateFinEncheres = dateFinEncheres;
		this.miseAPrix = miseAPrix;
		this.prixVente = prixVente;
		this.idUtilisateur = idUtilisateur;
		this.idCategorie = idCategorie;
		this.idRetrait = idRetrait;
		this.etatVente = etatVente;
	}

	// methode

	/**
	 * Methode renvoyant un boolean qui renvoie un boolean en fonction de la dateDebutEncheres et la date actuel 
	 * @return true si la dateDebutEncheres est après la date actuel sinon false
	 */
	public Boolean before() {
		System.out.println(LocalDate.now().isBefore(dateDebutEncheres));
		return LocalDate.now().isBefore(dateDebutEncheres);
	}

	/**
	 * Methode renvoyant un boolean qui renvoie un boolean en fonction de la dateDebutEncheres et la date actuel
	 * @return true si la dateDebutEncheres est avant la date actuel sinon false
	 */
	public Boolean after() {
		System.out.println(LocalDate.now().isAfter(dateDebutEncheres));
		return LocalDate.now().isBefore(dateDebutEncheres);
	}

	// Getters

	public Integer getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getDescription() {
		return description;
	}

	public LocalDate getDateDebutEncheres() {
		return dateDebutEncheres;
	}

	public LocalDate getDateFinEncheres() {
		return dateFinEncheres;
	}

	public Double getMiseAPrix() {
		return miseAPrix;
	}

	public Double getPrixVente() {
		return prixVente;
	}

	public Integer getIdUtilisateur() {
		return idUtilisateur;
	}

	public Integer getIdCategorie() {
		return idCategorie;
	}

	public Integer getIdRetrait() {
		return idRetrait;
	}

	// Setters

	public Integer getEtatVente() {
		return etatVente;
	}

	public void setEtatVente(Integer etatVente) {
		this.etatVente = etatVente;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setDateDebutEncheres(LocalDate dateDebutEncheres) {
		this.dateDebutEncheres = dateDebutEncheres;
	}

	public void setDateFinEncheres(LocalDate dateFinEncheres) {
		this.dateFinEncheres = dateFinEncheres;
	}

	public void setMiseAPrix(Double miseAPrix) {
		this.miseAPrix = miseAPrix;
	}

	public void setPrixVente(Double prixVente) {
		this.prixVente = prixVente;
	}

	public void setIdUtilisateur(Integer idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	public void setIdCategorie(Integer idCategorie) {
		this.idCategorie = idCategorie;
	}

	public void setIdRetrait(Integer idRetrait) {
		this.idRetrait = idRetrait;
	}

	// ToString

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Article [id=");
		builder.append(id);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", description=");
		builder.append(description);
		builder.append(", dateDebutEncheres=");
		builder.append(dateDebutEncheres);
		builder.append(", dateFinEncheres=");
		builder.append(dateFinEncheres);
		builder.append(", miseAPrix=");
		builder.append(miseAPrix);
		builder.append(", prixVente=");
		builder.append(prixVente);
		builder.append(", idUtilisateur=");
		builder.append(idUtilisateur);
		builder.append(", idCategorie=");
		builder.append(idCategorie);
		builder.append(", idRetrait=");
		builder.append(idRetrait);
		builder.append("]");
		return builder.toString();
	}

}
