<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@page import="java.time.LocalDate"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link
			href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
			rel="stylesheet"
			integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
			crossorigin="anonymous">
		<link rel="stylesheet" href="./css/eniEnchereV2.css">
		<script
			src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
			integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
			crossorigin="anonymous"></script>
		<title>EniEnchere</title>
	</head>
	<body>
		<%
		Utilisateur user = (Utilisateur) request.getSession().getAttribute("utilisateur");
		Article article = (Article) request.getAttribute("Article");
		Utilisateur acheteur = new Utilisateur();
		if (request.getAttribute("Acheteur") != null) {
			acheteur = (Utilisateur) request.getAttribute("Acheteur");
		}
		Integer etatArticle = article.getEtatVente();
		
		boolean estConnecte = false;
		if (user != null && user.getId() > 0) {
			estConnecte = true;
		}
		
		boolean aPremiereOffre = false;
		if (acheteur.getId() != null) {
			aPremiereOffre = true;
		}
		
		boolean EnchereEnCours = false;
		if (article.getDateFinEncheres().isAfter(LocalDate.now())) {
			EnchereEnCours = true;
		}
		
		boolean UserDiffVendeur = false;
		if (estConnecte && (user.getId() != article.getIdUtilisateur())) {
			UserDiffVendeur = true;
		}
		
		boolean UserDiffAcheteur = false;
		if (estConnecte && (user.getId() != acheteur.getId())) {
			UserDiffAcheteur = true;
		}
		%>
			<header class="global-nav">
				<a class="link-primary" href="Accueil">ENI-Enchères</a>
			</header>
		<div>
			<%
			// Si l'enchère n'est pas encore terminée ...
			if (EnchereEnCours) {
			%>
			<h2 class="textAlign">Détail vente</h2>
			<%
			// Si l'enchère est terminée et que l'utilisateur est l'acheteur ...
			} else if (!EnchereEnCours && !UserDiffAcheteur) {
			%>
			<h2 class="textAlign">Vous avez remporté la vente</h2>
			<%
			// Si l'enchère est terminée et que l'utilisateur n'est pas l'acheteur ...
			} else if (!EnchereEnCours && UserDiffAcheteur) {
			%>
			<h2 class="textAlign">${Acheteur.getPseudo()} a remporté la vente</h2>
			<%
			}
			%>
		</div>
		
		<div class="formulaire container-fluid border border-dark col-6">
			<form method="post" action="DisplayBid">
				<br>
				<!-- Affichage du nom de l'article -->
				<div>${Article.getNom()}</div>
		
				<!-- Affichage de la date de fin d'enchères -->
				<div>Description : ${Article.getDateFinEncheres()}</div>
		
				<!-- Affichage de la catégorie si l'enchère est en cours -->
				<%
				if (EnchereEnCours) {
				%>
					<div>Catégorie : ${Categorie.getLibelle()}</div>
				<%
				}
				%>
		
				<!-- Affichage de la mailleure offre s'il y en a une, et du pseudo de l'acheteur 
				si l'enchère est en cours et que ce n'est pas l'utilisateur -->
				<div>
					Meilleur offre :
					<%
				if (aPremiereOffre) {
				%>
					${Article.getPrixVente()} crédits
					<%
					if (EnchereEnCours || UserDiffAcheteur) {
					%>
					par ${Acheteur.getPseudo()}
				<%
					}
				} else {
				%>
					Pas encore d'enchère
				<%
				}
				%>
				</div>
		
				<!-- Affichage du prix initial de l'article -->
				<div>Mise à prix : ${Article.getMiseAPrix()} crédits</div>
		
				<!-- Affichage de la date de fin d'enchère si l'enchère n'est pas terminée et/ou si l'utilisateur n'est pas l'acheteur -->
				<%
				if (EnchereEnCours || UserDiffAcheteur) {
				%>
					<div>Fin de l'enchère : ${Article.getDateFinEncheres()}</div>
				<%
				}
				%>
		
				<!-- Affichage du retrait -->
				<div>Retrait : ${Retrait.getRue()} ${Retrait.getCp()}
					${Retrait.getVille()}</div>
		
				<!-- Affichage du pseudo du vendeur -->
				<div>Vendeur : ${Vendeur.getPseudo()}</div>
		
				<!-- Affichage du telephone du vendeur si l'enchère est terminée et que l'utilisateur est l'acheteur -->
				<%
				if (!EnchereEnCours || !UserDiffAcheteur) {
				%>
					<div>Tel : ${Vendeur.getTelephone()}</div>
				<%
				}
				%>
		
				<!-- Bouton pour enchérir si l'enchère est en cours et l'utilisateur n'est ni acheteur ni vendeur -->
				<%
				if (EnchereEnCours && UserDiffVendeur && UserDiffAcheteur) {
				%>
					<div>
						Ma proposition : <input type="number" id="prix" name="prix"
							min="${Article.getPrixVente() + 1}" max="${utilisateur.getCredit()}"></input>
						<button type="submit">Enchérir</button>
					</div>
				<%
				}
				%>
		
				<!-- Bouton pour retourner à l'accueil si l'enchère est terminée est que l'utilisateur est l'acheteur -->
				<%
				if (etatArticle == 0) {
					if (!EnchereEnCours && UserDiffAcheteur) {
				%>
						<div>
							<input type="button" name="back"
								onclick="window.location.href = 'Accueil';" value="Back" />
						</div>
				<%
					}
				} else {
					if (!EnchereEnCours && !UserDiffAcheteur) {
				%>
					<!-- Bouton pour retourner à l'accueil si l'enchère est terminée est que l'utilisateur est l'acheteur -->
						<div>
							<button type="submit" name="retrait">Retrait effectué</button>
						</div>
				<%
					}
				}
				%>
				<br> 
				<input type="hidden" name="idArticle" value="${Article.getId()}">
			</form>
		</div>
	
	</body>
</html>