package fr.eni.javaee.enienchere.bo;

public class Retrait {

	// Variables
	private Integer idRetrait;
	private String rue;
	private String cp;
	private String ville;

	// Constructeurs

	public Retrait() {
	}

	public Retrait(String rue, String cp, String ville) {
		super();
		this.rue = rue;
		this.cp = cp;
		this.ville = ville;
	}

	public Retrait(Integer idRetrait, String rue, String cp, String ville) {
		super();
		this.idRetrait = idRetrait;
		this.rue = rue;
		this.cp = cp;
		this.ville = ville;
	}

	// Getters

	public Integer getIdRetrait() {
		return idRetrait;
	}

	public String getRue() {
		return rue;
	}

	public String getCp() {
		return cp;
	}

	public String getVille() {
		return ville;
	}

	// Setters

	public void setIdRetrait(Integer idRetrait) {
		this.idRetrait = idRetrait;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	// ToString

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Retrait [idRetrait=");
		builder.append(idRetrait);
		builder.append(", rue=");
		builder.append(rue);
		builder.append(", cp=");
		builder.append(cp);
		builder.append(", ville=");
		builder.append(ville);
		builder.append("]");
		return builder.toString();
	}

}