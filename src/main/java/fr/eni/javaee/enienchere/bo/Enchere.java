package fr.eni.javaee.enienchere.bo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class Enchere implements Serializable {

	private static final long serialVersionUID = 1L;

	// Variables
	private Integer id;
	private LocalDate date;
	private Double montant;
	private Integer idArticle;
	private Integer idAcheteur;

	// Constructeurs

	public Enchere() {
	}

	public Enchere(LocalDate date, Double montant, Integer idArticle, Integer idAcheteur) {
		super();
		this.date = date;
		this.montant = montant;
		this.idArticle = idArticle;
		this.idAcheteur = idAcheteur;
	}
	
	public Enchere(Integer id, LocalDate date, Double montant, Integer idArticle, Integer idAcheteur) {
		super();
		this.id = id;
		this.date = date;
		this.montant = montant;
		this.idArticle = idArticle;
		this.idAcheteur = idAcheteur;
	}

	// Getters

	public Integer getId() {
		return id;
	}

	public LocalDate getDate() {
		return date;
	}

	public Double getMontant() {
		return montant;
	}

	public Integer getIdArticle() {
		return idArticle;
	}

	public Integer getIdAcheteur() {
		return idAcheteur;
	}

	// Setters

	public void setId(Integer id) {
		this.id = id;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public void setMontant(Double montant) {
		this.montant = montant;
	}

	public void setIdArticle(Integer idArticle) {
		this.idArticle = idArticle;
	}

	public void setIdAcheteur(Integer idAcheteur) {
		this.idAcheteur = idAcheteur;
	}

	// ToString

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Enchere [id=");
		builder.append(id);
		builder.append(", date=");
		builder.append(date);
		builder.append(", montant=");
		builder.append(montant);
		builder.append(", idArticle=");
		builder.append(idArticle);
		builder.append(", idAcheteur=");
		builder.append(idAcheteur);
		builder.append("]");
		return builder.toString();
	}

}
