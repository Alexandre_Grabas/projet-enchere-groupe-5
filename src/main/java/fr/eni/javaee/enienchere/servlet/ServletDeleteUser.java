package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ServletDeleteUser
 */
@WebServlet("/DeleteUser")
public class ServletDeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletDeleteUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/ModifieUser");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		Integer idUtilisateur = utilisateur.getId();
		EnchereManager enchereManager = new EnchereManager();
		List<Integer> listeCodesErreur = new ArrayList<>();

		if (idUtilisateur != null && idUtilisateur != 0) {
			try {
				enchereManager.supprimerUtilisateur(idUtilisateur);
				request.getSession().invalidate();
			} catch (EnchereException e) {
				System.out.println(e.getMessage());
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/ModifieUser");
				rd.forward(request, response);
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/Accueil");
			rd.forward(request, response);
		}

		RequestDispatcher rd = request.getRequestDispatcher("/Accueil");
		rd.forward(request, response);

	}

}
