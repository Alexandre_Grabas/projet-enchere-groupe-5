package fr.eni.javaee.enienchere.bo;

public class AppliTestBO {

	public static void main(String[] args) {

		Utilisateur u1 = new Utilisateur();
		u1.setPseudo("Gpadidai");
		u1.setNom("Padidai");
		u1.setPrenom("Gerard");
		u1.setEmail("gpadidai@gmail.com");
		u1.setTelephone("0648532178");
		u1.setRue("");
		u1.setVille("");
		u1.setCp("");
		u1.setMdp("P@ssw0rd");
		u1.setCredit(1248);
		u1.setAdministrateur(false);

		System.out.println(u1);

		Utilisateur u2 = new Utilisateur("pseudo2", "nom2", "prenom2", "email2", "tel2", "rue2", "ville2", "cp2",
				"mdp2", 1000, false);
		System.out.println(u2);

		Utilisateur u3 = new Utilisateur("pseudo3", "nom3", "prenom3", "email3", "tel3", "rue3", "ville3", "cp3",
				"mdp3");
		System.out.println(u3);
		
		Utilisateur u4=new Utilisateur();
		System.out.println(u4.getId());


	}

}
