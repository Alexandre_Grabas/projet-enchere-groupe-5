package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class Acceuil
 */
@WebServlet("/Accueil")
public class ServletAccueil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletAccueil() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EnchereManager manager = new EnchereManager();
		String AchatsVentes=request.getParameter("AchatsVentes");
		String filtre=request.getParameter("filtre");
		String categorie=request.getParameter("categorie");
		String enchereOuverte = request.getParameter("enchereOuverte");
		String mesEnchere = request.getParameter("mesEnchere");
		String mesEnchereRemportees = request.getParameter("mesEnchereRemportees");
		String mesVentesEnCours = request.getParameter("mesVentesEnCours");
		String ventesNonDebutees = request.getParameter("ventesNonDebutees");
		String ventesTerminees = request.getParameter("ventesTerminees");
		
		checkConectionUser(request,AchatsVentes);
		
		if (filtre==null && categorie==null) {
			filtre="";
			categorie="toute";
		}
		
		vue(request, manager, AchatsVentes, filtre, categorie, enchereOuverte, mesEnchere, mesEnchereRemportees,
				mesVentesEnCours, ventesNonDebutees, ventesTerminees);	
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/accueil.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		EnchereManager manager = new EnchereManager();
		String AchatsVentes=request.getParameter("AchatsVentes");
		String filtre=request.getParameter("filtre");
		String categorie=request.getParameter("categorie");
		String enchereOuverte = request.getParameter("enchereOuverte");
		String mesEnchere = request.getParameter("mesEnchere");
		String mesEnchereRemportees = request.getParameter("mesEnchereRemportees");
		String mesVentesEnCours = request.getParameter("mesVentesEnCours");
		String ventesNonDebutees = request.getParameter("ventesNonDebutees");
		String ventesTerminees = request.getParameter("ventesTerminees");
		
		
		if(request.getParameter("deconnexion")!=null){
			request.getSession().invalidate();
		}
		checkConectionUser(request,AchatsVentes);
		
		if (filtre==null && categorie==null) {
			filtre="";
			categorie="toute";
		}
		
		vue(request, manager, AchatsVentes, filtre, categorie, enchereOuverte, mesEnchere, mesEnchereRemportees,
				mesVentesEnCours, ventesNonDebutees, ventesTerminees);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/accueil.jsp");
		rd.forward(request, response);
	}
	
	/**
	 * Methode appelant les methode permettant d'obtenir la bonne liste d'article en fonction de si l'utilisateur est connecter ou non
    * @param AchatsVentes etat du radio bouton Achats/Ventes
    * @param filtre chaine de caractère contenant le masque a trouver dans le nom de l'article
    * @param categorie catégorie de l'article
    * @param request HttpServletRequest request de la servlet 
    * @param manager manager de la class EnchereManager de la bll
    * @param encheresOuvertes "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesEncheres "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesEncheresRemportees "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesVentesEnCours "on" ou null en fonction de l'etats du bouton de la jsp
    * @param ventesNonDebutees "on" ou null en fonction de l'etats du bouton de la jsp
    * @param ventesTerminees "on" ou null en fonction de l'etats du bouton de la jsp
	 */
	private void vue(HttpServletRequest request, EnchereManager manager, String AchatsVentes, String filtre,
			String categorie, String enchereOuverte, String mesEnchere, String mesEnchereRemportees,
			String mesVentesEnCours, String ventesNonDebutees, String ventesTerminees) {
		List<Article> listeArticle;
		HttpSession session = request.getSession();
		if(session.getAttribute("utilisateur") !=null) {
			listeArticle=vueArticle(AchatsVentes,filtre,categorie,request,manager,enchereOuverte,mesEnchere,mesEnchereRemportees,mesVentesEnCours,ventesNonDebutees,ventesTerminees);
		}else {
			listeArticle=manager.selectFiltredArticle(filtre, categorie);
			setListUserOfListArticle(request, manager, listeArticle);
		}
		request.setAttribute("ListeArticle", listeArticle);
	}
	
   /**
    * Methode créant la liste d'affichage des enchère en fonction des paramètres referant a l'etats des differents boutons de la jsp accueil
    * @param AchatsVentes etat du radio bouton Achats/Ventes
    * @param filtre chaine de caractère contenant le masque a trouver dans le nom de l'article
    * @param categorie catégorie de l'article
    * @param request HttpServletRequest request de la servlet 
    * @param manager manager de la class EnchereManager de la bll
    * @param encheresOuvertes "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesEncheres "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesEncheresRemportees "on" ou null en fonction de l'etats du bouton de la jsp
    * @param mesVentesEnCours "on" ou null en fonction de l'etats du bouton de la jsp
    * @param ventesNonDebutees "on" ou null en fonction de l'etats du bouton de la jsp
    * @param ventesTerminees "on" ou null en fonction de l'etats du bouton de la jsp
    * @return
    */
	private List<Article> vueArticle(String AchatsVentes,String filtre,String categorie,HttpServletRequest request, EnchereManager manager,String encheresOuvertes,String mesEncheres,String mesEncheresRemportees,String mesVentesEnCours,String ventesNonDebutees,String ventesTerminees){
        List<Article> listeArticle= new ArrayList<>();
        if (AchatsVentes == null || AchatsVentes.equals("Achats")){
        	if(AchatsVentes == null) {
        		encheresOuvertes="on";
        	}
        	listeArticle=vueAchats(filtre,categorie,request,manager,encheresOuvertes,mesEncheres,mesEncheresRemportees);
        }else {
        	listeArticle=vueVentes(filtre,categorie,request,manager,mesVentesEnCours,ventesNonDebutees,ventesTerminees);
        }
        setListUserOfListArticle(request, manager, listeArticle);
        return listeArticle;
        
    }
    
	/**
	 * Methode créant la liste d'affichage des enchère en fonction des paramètres referant a l'etats des differents boutons de la jsp accueil
	 * lorsque le radio bouton Achats/Ventes est sur Achats
     * @param filtre chaine de caractère contenant le masque a trouver dans le nom de l'article
     * @param categorie catégorie de l'article
     * @param request HttpServletRequest request de la servlet
     * @param manager manager de la class EnchereManager de la bll
     * @param encheresOuvertes "on" ou null en fonction de l'etats du bouton de la jsp
     * @param mesEncheres "on" ou null en fonction de l'etats du bouton de la jsp
     * @param mesEncheresRemportees "on" ou null en fonction de l'etats du bouton de la jsp
	 * @return
	 */
	private List<Article> vueAchats(String filtre,String categorie,HttpServletRequest request, EnchereManager manager,String encheresOuvertes,String mesEncheres,String mesEncheresRemportees){
	    List<Article> listeArticle= new ArrayList<>();
	    Integer id_utilisateur=((Utilisateur) request.getSession().getAttribute("utilisateur")).getId();
	
	    if(encheresOuvertes!=null && mesEncheres==null && mesEncheresRemportees==null) {
	        
	        listeArticle=manager.selectArticleFiltrerConected(id_utilisateur,filtre, categorie);
	        
	    }else if(encheresOuvertes!=null && mesEncheres!=null && mesEncheresRemportees==null) {
	        
	        listeArticle=manager.selectArticleFiltrerConected(id_utilisateur,filtre, categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserFiltrer(id_utilisateur,filtre, categorie));
	        
	    }else if(encheresOuvertes!=null && mesEncheres==null && mesEncheresRemportees!=null) {
	        
	        listeArticle=manager.selectArticleFiltrerConected(id_utilisateur,filtre, categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserFiltrerWin(id_utilisateur,filtre, categorie));
	        
	    }else if(encheresOuvertes!=null && mesEncheres!=null && mesEncheresRemportees!=null) {
	        
	        listeArticle=manager.selectArticleFiltrerConected(id_utilisateur,filtre, categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserFiltrer(id_utilisateur,filtre, categorie));
	        listeArticle.addAll(manager.selectArticleEnchereByUserFiltrerWin(id_utilisateur,filtre, categorie));
	        
	    }
	    
	    else if(encheresOuvertes==null && mesEncheres!=null && mesEncheresRemportees==null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserFiltrer(id_utilisateur,filtre, categorie);
	        
	    }else if(encheresOuvertes==null && mesEncheres==null && mesEncheresRemportees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserFiltrerWin(id_utilisateur,filtre, categorie);
	        
	    }else if(encheresOuvertes==null && mesEncheres!=null && mesEncheresRemportees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserFiltrer(id_utilisateur,filtre, categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserFiltrerWin(id_utilisateur,filtre, categorie));
	        
	    }else if(encheresOuvertes==null && mesEncheres==null && mesEncheresRemportees==null) {
	        
	    }
	    listeArticle=removeDuplicate(listeArticle);
	    return listeArticle;
	}
    
	/**
	 * Methode créant la liste d'affichage des enchère en fonction des paramètres referant a l'etats des differents boutons de la jsp accueil
	 * lorsque le radio bouton Achats/Ventes est sur Ventes
     * @param filtre chaine de caractère contenant le masque a trouver dans le nom de l'article
     * @param categorie catégorie de l'article
     * @param request HttpServletRequest request de la servlet
     * @param manager manager de la class EnchereManager de la bll
     * @param mesVentesEnCours "on" ou null en fonction de l'etats du bouton de la jsp
     * @param ventesNonDebutees "on" ou null en fonction de l'etats du bouton de la jsp
     * @param ventesTerminees "on" ou null en fonction de l'etats du bouton de la jsp
	 * @return
	 */
	private List<Article> vueVentes(String filtre,String categorie,HttpServletRequest request, EnchereManager manager,String mesVentesEnCours,String ventesNonDebutees,String ventesTerminees){
	    List<Article> listeArticle= new ArrayList<>();
	    Integer idUtilisateur=((Utilisateur) request.getSession().getAttribute("utilisateur")).getId();
	    if(mesVentesEnCours!=null && ventesNonDebutees==null && ventesTerminees==null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingInProgress(idUtilisateur,filtre,categorie);
	        
	        
	    }else if(mesVentesEnCours!=null && ventesNonDebutees!=null && ventesTerminees==null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingInProgress(idUtilisateur,filtre,categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserSelingNotStarted(idUtilisateur,filtre,categorie));
	        
	        
	    }else if(mesVentesEnCours!=null && ventesNonDebutees==null && ventesTerminees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingInProgress(idUtilisateur,filtre,categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserSelingEnded(idUtilisateur,filtre,categorie));
	        
	        
	    }else if(mesVentesEnCours!=null && ventesNonDebutees!=null && ventesTerminees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingInProgress(idUtilisateur,filtre,categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserSelingNotStarted(idUtilisateur,filtre,categorie));
	        listeArticle.addAll(manager.selectArticleEnchereByUserSelingEnded(idUtilisateur,filtre,categorie));
	        
	    }
	    
	    else if(mesVentesEnCours==null && ventesNonDebutees!=null && ventesTerminees==null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingNotStarted(idUtilisateur,filtre,categorie);
	        
	        
	    }else if(mesVentesEnCours==null && ventesNonDebutees==null && ventesTerminees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingEnded(idUtilisateur,filtre,categorie);
	        
	        
	    }else if(mesVentesEnCours==null && ventesNonDebutees!=null && ventesTerminees!=null) {
	        
	        listeArticle=manager.selectArticleEnchereByUserSelingNotStarted(idUtilisateur,filtre,categorie);
	        listeArticle.addAll(manager.selectArticleEnchereByUserSelingEnded(idUtilisateur,filtre,categorie));
	    
	    }
	    
	    listeArticle=removeDuplicate(listeArticle); 
	        
	    return listeArticle;
	}


	/**
	 * Methode envoyant les donnée nessaisaire au bon affichage des etats des boutons dans la jsp a HttpServletRequest request de la servet
	 * @param request HttpServletRequest request de la servlet 
	 * @param AchatsVentes etat du radio bouton Achats/Ventes
	 */
	private void initRadioCheckBoxChoice(HttpServletRequest request,String AchatsVentes) {
		boolean premier;
		
		if(AchatsVentes!=null) {
			premier=false;
			request.setAttribute("AchatsVentes",AchatsVentes);
		}else {
			AchatsVentes="Achats";
			premier=true;
			request.setAttribute("AchatsVentes",AchatsVentes);
		}
		
		if(AchatsVentes.equals("Achats")){
			if(request.getParameter("enchereOuverte")!=null) {
				
				request.setAttribute("enchereOuverte","checked");
			}else {
				if(premier) {
					request.setAttribute("enchereOuverte","checked");
				}else {
					request.setAttribute("enchereOuverte","");
				}
			}
			
			if(request.getParameter("mesEnchere")!=null) {
				request.setAttribute("mesEnchere","checked");
			}else {
				request.setAttribute("mesEnchere","");
			}
			
			if(request.getParameter("mesEnchereRemportees")!=null) {
				request.setAttribute("mesEnchereRemportees","checked");
			}else {
				request.setAttribute("mesEnchereRemportees","");
			}
		
		}else {
			
			if(request.getParameter("mesVentesEnCours")!=null) {
				request.setAttribute("mesVentesEnCours","checked");
			}else {
				request.setAttribute("mesVentesEnCours","");
			}
			
			if(request.getParameter("ventesNonDebutees")!=null) {
				request.setAttribute("ventesNonDebutees","checked");
			}else {
				request.setAttribute("ventesNonDebutees","");
			}
	
			if(request.getParameter("ventesTerminees")!=null) {
				request.setAttribute("ventesTerminees","checked");
			}else {
				request.setAttribute("ventesTerminees","");
			}
		}
		
	}
	
	/**
	 * Methode vérifiant si un utilisateur est connecter et envoyant les donnée nessesaire a HttpServletRequest request de la servet pour l'affichage dans la jsp
	 * @param request HttpServletRequest request de la servlet 
	 * @param AchatsVentes etat du radio bouton Achats/Ventes
	 */
	private void checkConectionUser(HttpServletRequest request,String AchatsVentes) {
		HttpSession session = request.getSession();
		if(session.getAttribute("utilisateur") !=null) {
			request.setAttribute("connecter", "visible");
			request.setAttribute("deconnecter", "hidden");
			initRadioCheckBoxChoice(request,AchatsVentes);
		}
		else {
			request.setAttribute("connecter", "hidden");
			request.setAttribute("deconnecter", "visible");
		}
	}
	
	/** 
	 * Methode créant une liste d'utilisateur vendant les article de la liste d'article rentrer en paramétre et la passant a HttpServletRequest request de la servet
     * @param request HttpServletRequest request de la servlet 
     * @param manager manager de la class EnchereManager de la bll
	 * @param listeArticle liste d'article 
	 */
	private void setListUserOfListArticle(HttpServletRequest request, EnchereManager manager,
			List<Article> listeArticle) {
		if (!(listeArticle.isEmpty())) {
			List<Utilisateur> listeUtilisateur = new ArrayList<>();
			for (Article article : listeArticle) {
				listeUtilisateur.add(manager.selectUtilisateurById(article.getIdUtilisateur()));
			}
			request.setAttribute("ListeUtilisateur", listeUtilisateur);
		}
	}
	
	/**
	 * Methode permettant de réccuperer tout les article en vente et de les mettre en attribut dans request
	 * @param request HttpServletRequest request de la servlet 
     * @param manager manager de la class EnchereManager de la bll
	 * @param listeArticle
	 */
	private List<Article> getAllArticle(HttpServletRequest request, EnchereManager manager) {
		List<Article> listeArticle= manager.selectAllArticle();
		setListUserOfListArticle(request, manager, listeArticle);
		return listeArticle;
	}
	
    /*quand un nouvel id utilisateur est trouver !set.add(p.getId() renvoie false
    sinon il renvoie true*/
	/**
	 * Methode qui permet de supprimer les valeur en double dans une liste
	 * @param ld liste contenant des valeur en double
	 * @return La liste rentrer en paramètre avec des valeur unique
	 */
   public static List<Article> removeDuplicate(List<Article> ld){
       Set<Integer> set = new HashSet<>(ld.size());
       ld.removeIf(p -> !set.add(p.getId()));
       return ld;
   }
   
	
}
