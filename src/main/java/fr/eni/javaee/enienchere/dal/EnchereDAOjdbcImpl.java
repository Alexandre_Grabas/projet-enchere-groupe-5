package fr.eni.javaee.enienchere.dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Categorie;
import fr.eni.javaee.enienchere.bo.Enchere;
import fr.eni.javaee.enienchere.bo.Retrait;
import fr.eni.javaee.enienchere.bo.Utilisateur;

public class EnchereDAOjdbcImpl implements EnchereDAO {

	// Requêtes sur la table `utilisateur`

	private static final String SELECT_USER_BY_EMAIL_OR_PSEUDO = "SELECT * FROM `utilisateur` WHERE (`pseudo`=? OR `email`=?) AND `mot_de_passe`=?;";
	private static final String SELECT_USER_BY_ID = "SELECT * FROM `utilisateur` WHERE `id_utilisateur`=?;";

	private static final String INSERT_USER = "INSERT INTO `utilisateur` (`pseudo`, `nom`, `prenom`, `email`, `telephone`, `rue`, `cp`, `ville`, `mot_de_passe`, `credit`, `administrateur`) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	private static final String UPDATE_USER_BY_ID = "UPDATE `utilisateur` SET `pseudo` = ?, `nom` = ?, `prenom` = ?, `email` = ?, `telephone` = ?, `rue` = ?, `cp` = ?, `ville` = ?, `mot_de_passe` = ? "
			+ "WHERE `id_utilisateur` = ?;";
	private static final String UPDATE_CREDIT_USER_BY_ID = "UPDATE `utilisateur` SET `credit` = ? WHERE `id_utilisateur` = ?;";

	private static final String DELETE_USER_BY_ID = "DELETE FROM `utilisateur` WHERE `id_utilisateur`=?;";

	// Requêtes sur la table `article`

	private static final String SELECT_ALL_ARTICLE = "SELECT * FROM `article_vendu`;";
	private static final String SELECT_ARTICLE_BY_ID = "SELECT * FROM `article_vendu` WHERE `id_article` =?;";
	private static final String SELECT_ARTICLE_IN_SALE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_CATEGORIE_IN_SALE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle`=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_FILTRE_IN_SALE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `nom_article` LIKE ? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_FILTRE_CATEGORIE_IN_SALE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` on `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle`=? AND `nom_article` LIKE ? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_CONECTED = "SELECT * FROM `article_vendu` "
			+ "WHERE `id_vendeur`!=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_CATEGORIE_CONECTED = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle`=? AND `id_vendeur`!=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_FILTRE_CONECTED = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `nom_article` LIKE ? AND `id_vendeur`!=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_FILTRE_CATEGORIE_CONECTED = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` on `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle`=? AND `nom_article` LIKE ? AND `id_vendeur`!=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;;";
	private static final String SELECT_ARTICLE_ENCHERE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article " + "WHERE id_acheteur=?;";
	private static final String SELECT_ARTICLE_CATEGORIE_ENCHERE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `libelle`=? AND id_acheteur=?;";
	private static final String SELECT_ARTICLE_FILTRE_ENCHERE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `nom_article` LIKE ? AND id_acheteur=?;";
	private static final String SELECT_ARTICLE_FILTRE_CATEGORIE_ENCHERE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `libelle`=? AND `nom_article` LIKE ? AND id_acheteur=?;";
	private static final String SELECT_ARTICLE_FILTRE_WIN = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE id_acheteur=? and prix_vente=montant_enchere and DATEDIFF(date_fin_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_CATEGORIE_ENCHERE_WIN = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `libelle`=? AND id_acheteur=? and prix_vente=montant_enchere and DATEDIFF(date_fin_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_FILTRE_ENCHERE_WIN = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `nom_article` LIKE ? AND id_acheteur=? and prix_vente=montant_enchere and DATEDIFF(date_fin_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_FILTRE_CATEGORIE_ENCHERE_WIN = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "INNER JOIN enchere ON article_vendu.id_article = enchere.id_article "
			+ "WHERE `libelle`=? AND `nom_article` LIKE ? AND id_acheteur=? and prix_vente=montant_enchere and DATEDIFF(date_fin_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_IN_PROGRESS = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_IN_PROGRESS_CATEGORIE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle` = ? AND `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF(date_fin_enchere,?)>0 AND DATEDIFF(date_debut_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_NOT_STARTED = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF (date_debut_enchere,?)>=0 ;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_NOT_STARTED_CATEGORIE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle` =? AND `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF (date_debut_enchere,?)>=0;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_ENDED = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF(date_fin_enchere,?)<=0;";
	private static final String SELECT_ARTICLE_ENCHERE_BY_USER_SELING_ENDED_CATEGORIE = "SELECT * FROM `article_vendu` "
			+ "INNER JOIN `categorie` ON `article_vendu`.`id_categorie` = `categorie`.`id_categorie` "
			+ "WHERE `libelle` =? AND `nom_article` LIKE ? AND id_vendeur=? AND DATEDIFF(date_fin_enchere,?)<=0 ;";

	private static final String INSERT_ARTICLE = "INSERT INTO `article_vendu` (`nom_article`, `description`, `date_debut_enchere`, `date_fin_enchere`, `prix_initial`, `prix_vente`, `id_vendeur`, `id_categorie`, `id_retrait`) "
			+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";

	private static final String UPDATE_ARTICLE = "UPDATE `article_vendu` SET `nom_article` = ?, `description` = ?, `date_debut_enchere` = ?, `date_fin_enchere` = ?, `prix_initial` = ?, `prix_vente` = ?, `id_categorie` = ?, `id_retrait` = ? WHERE `id_article` = ?;";
	private static final String UPDATE_PRIX_ARTICLE_BY_ID = "UPDATE `article_vendu` SET `prix_vente` = ? WHERE `id_article` = ?;";
	private static final String UPDATE_ETAT_ARTICLE_BY_ID = "UPDATE `article_vendu` SET `etat_vente` = ? WHERE `id_article` = ?;";

	private static final String DELETE_ARTICLE_BY_ID = "DELETE FROM `article_vendu` WHERE `id_article`=?;";

	// Requêtes sur la table `categorie`

	private static final String SELECT_ALL_CATEGORIES = "SELECT * FROM `categorie`;";
	private static final String SELECT_CATEGORIE_BY_ID = "SELECT * FROM `categorie` WHERE `id_categorie`=?";

	// Requêtes sur la table `retrait`

	private static final String SELECT_SPECIFIC_RETRAIT = "SELECT * FROM `retrait` WHERE `rue` = ? AND `cp` = ? AND `ville` = ?;";
	private static final String SELECT_RETRAIT_BY_ID = "SELECT * FROM `retrait` WHERE `id_retrait`=?";

	private static final String INSERT_RETRAIT = "INSERT INTO `retrait` (`rue`, `cp`, `ville`) VALUES (?, ? ,?)";

	// Requêtes sur la table `enchere`

	private static final String SELECT_ENCHERE_BY_ID_ARTICLE = "SELECT * FROM `enchere` WHERE `id_article` = ? ORDER BY `montant_enchere` DESC LIMIT 1;";

	private static final String INSERT_ENCHERE = "INSERT INTO `enchere` (`date_enchere`, `montant_enchere`, `id_article`, `id_acheteur`) VALUES (?, ? ,?, ?);";

	private static final String DELETE_ENCHERE_BY_ID = "DELETE FROM `enchere` WHERE `id_enchere`=?;";

	// Methodes pour `utilisateur`

	@Override
	public Utilisateur selectUserById(Integer idUtilisateur) {

		Utilisateur user = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_USER_BY_ID);

			// Ajout dans la requête de l'id spécifié
			pstmt.setInt(1, idUtilisateur);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {

				// Création d'un nouvel utilisateur avec les données récupérées par la requête
				user = new Utilisateur(resultatUser.getInt("id_utilisateur"), resultatUser.getString("pseudo"),
						resultatUser.getString("nom"), resultatUser.getString("prenom"),
						resultatUser.getString("email"), resultatUser.getString("telephone"),
						resultatUser.getString("rue"), resultatUser.getString("ville"), resultatUser.getString("cp"),
						resultatUser.getString("mot_de_passe"), resultatUser.getInt("credit"),
						resultatUser.getBoolean("administrateur"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	@Override
	public Utilisateur selectUserByEmailOrPseudo(String identifiant, String pwd) {

		Utilisateur user = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_USER_BY_EMAIL_OR_PSEUDO);

			// Ajout dans la requête de l'identifiant et du mot de passe spécifié
			pstmt.setString(1, identifiant);
			pstmt.setString(2, identifiant);
			pstmt.setString(3, pwd);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {

				// Création d'un nouvel utilisateur avec les données récupérées par la requêtes
				user = new Utilisateur(resultatUser.getInt("id_utilisateur"), resultatUser.getString("pseudo"),
						resultatUser.getString("nom"), resultatUser.getString("prenom"),
						resultatUser.getString("email"), resultatUser.getString("telephone"),
						resultatUser.getString("rue"), resultatUser.getString("ville"), resultatUser.getString("cp"),
						resultatUser.getString("mot_de_passe"), resultatUser.getInt("credit"),
						resultatUser.getBoolean("administrateur"));

			} else {
				System.out.println("Aucun utilisateur trouvé dans la base: identifiant ou mot de passe invalide");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return user;
	}

	@Override
	public Integer insertUser(Utilisateur utilisateur) {

		Integer ID = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);

			// Ajout dans la requêtes des données de l'utilisateur spécifié
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6, utilisateur.getRue());
			pstmt.setString(7, utilisateur.getCp());
			pstmt.setString(8, utilisateur.getVille());
			pstmt.setString(9, utilisateur.getMdp());
			pstmt.setInt(10, utilisateur.getCredit());
			pstmt.setBoolean(11, utilisateur.isAdministrateur());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();

			// S'il y a un résultat ...
			if (rs.next()) {
				// Auto-incrémentation de l'id
				ID = rs.getInt(1);
				utilisateur.setId(ID);
				rs.close();
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ID;
	}

	@Override
	public void deleteUserById(Integer idUtilisateur) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_USER_BY_ID);

			// Ajout dans la requête de l'id spécifié
			pstmt.setInt(1, idUtilisateur);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateUserById(Utilisateur utilisateur) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_USER_BY_ID);

			// Ajout dans la requête des données de l'utilisateur spécifié
			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6, utilisateur.getRue());
			pstmt.setString(7, utilisateur.getCp());
			pstmt.setString(8, utilisateur.getVille());
			pstmt.setString(9, utilisateur.getMdp());
			pstmt.setInt(10, utilisateur.getId());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updateCreditUserById(Integer idUtilisateur, Integer credit) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_CREDIT_USER_BY_ID);

			// Ajout dans la requête de l'id spécifié et du nombre de crédit
			pstmt.setString(1, credit + "");
			pstmt.setString(2, idUtilisateur + "");
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Methodes pour `article`

	@Override
	public List<Article> selectAllArticle() {

		List<Article> listeArticle = new ArrayList<>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_ARTICLE);
			ResultSet rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {

				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), (rs.getDate("date_debut_enchere")).toLocalDate(),
						(rs.getDate("date_fin_enchere")).toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public Article selectArticleById(Integer idArticle) {

		Article article = new Article();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_BY_ID);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idArticle);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {

				// Création d'un nouvel article avec les données récupérées par la requête
				article = new Article(resultatUser.getInt("id_article"), resultatUser.getString("nom_article"),
						resultatUser.getString("description"),
						(resultatUser.getDate("date_debut_enchere")).toLocalDate(),
						(resultatUser.getDate("date_fin_enchere")).toLocalDate(),
						resultatUser.getDouble("prix_initial"), resultatUser.getDouble("prix_vente"),
						resultatUser.getInt("id_vendeur"), resultatUser.getInt("id_categorie"),
						resultatUser.getInt("id_retrait"),resultatUser.getInt("etat_vente"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return article;
	}

	@Override
	public List<Article> selectArticleFiltrer(String filtreNom, String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			// Si le filtre est vide et qu'on ne veut pas toutes les catégories ...
			if (filtreNom.equals("") && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_CATEGORIE_IN_SALE);
				pstmt.setString(1, categorie);
				pstmt.setDate(2, Date.valueOf(LocalDate.now()));
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on veut toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && categorie.equals("toute")) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_IN_SALE);
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setDate(2, Date.valueOf(LocalDate.now()));
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on ne veut pas toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_CATEGORIE_IN_SALE);
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Sinon ...
			} else {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_IN_SALE);
				pstmt.setDate(1, Date.valueOf(LocalDate.now()));
				pstmt.setDate(2, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();
			}

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public List<Article> selectArticleFiltrerConected(Integer idUtilisateur, String filtreNom, String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;
		PreparedStatement pstmt;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			// Si le filtre est vide et qu'on ne veut pas toutes les catégories ...
			if (filtreNom.equals("") && (!(categorie.equals("toute")))) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_CATEGORIE_CONECTED);
				pstmt.setString(1, categorie);
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));

				// Si le filtre n'est pas vide et qu'on veut toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && categorie.equals("toute")) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_CONECTED);
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));

				// Si le filtre n'est pas vide et qu'on ne veut pas toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && (!(categorie.equals("toute")))) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_CATEGORIE_CONECTED);
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
				pstmt.setDate(5, Date.valueOf(LocalDate.now()));

				// Sinon ...
			} else {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_CONECTED);
				pstmt.setInt(1, idUtilisateur);
				pstmt.setDate(2, Date.valueOf(LocalDate.now()));
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
			}
			rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public List<Article> selectArticleEnchereByUserFiltrer(Integer idUtilisateur, String filtreNom, String categorie) {
		List<Article> listeArticle = new ArrayList<>();

		ResultSet rs;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			// Si le filtre est vide et qu'on ne veut pas toutes les catégories ...
			if (filtreNom.equals("") && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_CATEGORIE_ENCHERE);
				pstmt.setString(1, categorie);
				pstmt.setInt(2, idUtilisateur);
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on veut toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && categorie.equals("toute")) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_ENCHERE);
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on ne veut pas toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_CATEGORIE_ENCHERE);
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				rs = pstmt.executeQuery();

				// Sinon ...
			} else {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE);
				pstmt.setInt(1, idUtilisateur);
				rs = pstmt.executeQuery();
			}

			// Tant qu'il y a un résultat ...
			while (rs.next()) {

				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public List<Article> selectArticleEnchereByUserFiltrerWin(Integer idUtilisateur, String filtreNom,
			String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			// Si le filtre est vide et qu'on ne veut pas toutes les catégories ...
			if (filtreNom.equals("") && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_CATEGORIE_ENCHERE_WIN);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, categorie);
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on veut toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && categorie.equals("toute")) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_ENCHERE_WIN);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Si le filtre n'est pas vide et qu'on ne veut pas toutes les catégories ...
			} else if ((!(filtreNom.equals(""))) && (!(categorie.equals("toute")))) {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_CATEGORIE_ENCHERE_WIN);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();

				// Sinon ...
			} else {
				PreparedStatement pstmt = cnx.prepareStatement(SELECT_ARTICLE_FILTRE_WIN);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setInt(1, idUtilisateur);
				pstmt.setDate(2, Date.valueOf(LocalDate.now()));
				rs = pstmt.executeQuery();
			}

			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return listeArticle;
	}

	@Override
	public List<Article> selectArticleEnchereByUserSelingInProgress(Integer idUtilisateur, String filtreNom,
			String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;
		PreparedStatement pstmt;

		try (Connection cnx = ConnectionProvider.getConnection()) {

			// Si on veut toutes les catégories ...
			if (categorie.equals("toute")) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_IN_PROGRESS);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));

			} else {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_IN_PROGRESS_CATEGORIE);

				// Ajout des valeurs spécifiées dans l'autre requête
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
				pstmt.setDate(5, Date.valueOf(LocalDate.now()));
			}
			rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public List<Article> selectArticleEnchereByUserSelingNotStarted(Integer idUtilisateur, String filtreNom,
			String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;
		PreparedStatement pstmt;

		try (Connection cnx = ConnectionProvider.getConnection()) {

			// Si on veut toutes les catégories ...
			if (categorie.equals("toute")) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_NOT_STARTED);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));

			} else {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_NOT_STARTED_CATEGORIE);

				// Ajout des valeurs spécifiées dans l'autre requête
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
			}
			rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public List<Article> selectArticleEnchereByUserSelingEnded(Integer idUtilisateur, String filtreNom,
			String categorie) {

		List<Article> listeArticle = new ArrayList<>();
		ResultSet rs;
		PreparedStatement pstmt;

		try (Connection cnx = ConnectionProvider.getConnection()) {

			// Si on veut toutes les catégories ...
			if (categorie.equals("toute")) {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_ENDED);

				// Ajout des valeurs spécifiées dans la requête
				pstmt.setString(1, "%" + filtreNom + "%");
				pstmt.setInt(2, idUtilisateur);
				pstmt.setDate(3, Date.valueOf(LocalDate.now()));

				// Sinon ...
			} else {
				pstmt = cnx.prepareStatement(SELECT_ARTICLE_ENCHERE_BY_USER_SELING_ENDED_CATEGORIE);

				// Ajout des valeurs spécifiées dans l'autre requête
				pstmt.setString(1, categorie);
				pstmt.setString(2, "%" + filtreNom + "%");
				pstmt.setInt(3, idUtilisateur);
				pstmt.setDate(4, Date.valueOf(LocalDate.now()));
			}
			rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste d'article d'un nouvel article créé avec les données
				// récupérées d'une ligne
				Article ajout = new Article(rs.getInt("id_article"), rs.getString("nom_article"),
						rs.getString("description"), rs.getDate("date_debut_enchere").toLocalDate(),
						rs.getDate("date_fin_enchere").toLocalDate(), rs.getDouble("prix_initial"),
						rs.getDouble("prix_vente"), rs.getInt("id_vendeur"), rs.getInt("id_categorie"),
						rs.getInt("id_retrait"));
				listeArticle.add(ajout);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listeArticle;
	}

	@Override
	public Integer insertArticle(Article article) {

		Integer ID = null;
		ZoneId defaultZoneId = ZoneId.systemDefault();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_ARTICLE, PreparedStatement.RETURN_GENERATED_KEYS);

			// Ajout des valeurs de l'article spécifié dans la requête
			pstmt.setString(1, article.getNom());
			pstmt.setString(2, article.getDescription());
			pstmt.setDate(3, new java.sql.Date(
					Date.from((article.getDateDebutEncheres()).atStartOfDay(defaultZoneId).toInstant()).getTime()));
			pstmt.setDate(4, new java.sql.Date(
					Date.from((article.getDateFinEncheres()).atStartOfDay(defaultZoneId).toInstant()).getTime()));
			pstmt.setDouble(5, article.getMiseAPrix());
			pstmt.setDouble(6, article.getPrixVente());
			pstmt.setInt(7, article.getIdUtilisateur());
			pstmt.setInt(8, article.getIdCategorie());
			pstmt.setInt(9, article.getIdRetrait());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();

			// S'il y a un résultat ...
			if (rs.next()) {
				// Auto-incrémentation de l'id
				ID = rs.getInt(1);
				article.setId(ID);
				rs.close();
				pstmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return ID;
	}

	public void updateArticle(Article article) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_ARTICLE);

			// Ajout des valeurs de l'article spécifié dans la requête
			pstmt.setString(1, article.getNom());
			pstmt.setString(2, article.getDescription());
			pstmt.setDate(3, java.sql.Date.valueOf(article.getDateDebutEncheres()));
			pstmt.setDate(4, java.sql.Date.valueOf(article.getDateFinEncheres()));
			pstmt.setDouble(5, article.getMiseAPrix());
			pstmt.setDouble(6, article.getPrixVente());
			pstmt.setInt(7, article.getIdCategorie());
			pstmt.setInt(8, article.getIdRetrait());
			pstmt.setInt(9, article.getId());
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void updatePrixArticleById(Integer idArticle, Double prixVente) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_PRIX_ARTICLE_BY_ID);

			// Ajout de l'id et du prix de vente spécifié dans la requête
			pstmt.setString(1, prixVente + "");
			pstmt.setString(2, idArticle + "");
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateEtatArticleById(Integer idArticle, Integer etatVente) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(UPDATE_ETAT_ARTICLE_BY_ID);

			// Ajout de l'id et de l'état de vente spécifiés dans la requête
			pstmt.setString(1, etatVente + "");
			pstmt.setString(2, idArticle + "");
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void deleteArticleById(Integer idArticle) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_ARTICLE_BY_ID);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idArticle);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// Methodes pour `categorie`

	@Override
	public List<Categorie> selectAllCategories() {

		List<Categorie> categories = new ArrayList<Categorie>();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ALL_CATEGORIES);
			ResultSet rs = pstmt.executeQuery();

			// Tant qu'il y a un résultat ...
			while (rs.next()) {
				// Ajout dans la liste de catégories d'une nouvelle catégorie créée avec les
				// données d'une ligne récupérée
				categories.add(new Categorie(rs.getInt("id_categorie"), rs.getString("libelle")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categories;
	}

	@Override
	public Categorie selectCategorieByID(Integer idCategorie) {

		Categorie categorie = new Categorie();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_CATEGORIE_BY_ID);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idCategorie);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {
				// Création d'une nouvelle catégorie avec les données récupérées
				categorie = new Categorie(idCategorie, resultatUser.getString("libelle"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return categorie;
	}

	// Methodes pour `retrait`

	@Override
	public Integer selectSpecificRetrait(Retrait retrait) {

		Integer ID = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_SPECIFIC_RETRAIT);

			// Ajout des valeurs du retrait spécifié dans la requête
			pstmt.setString(1, retrait.getRue());
			pstmt.setString(2, retrait.getCp());
			pstmt.setString(3, retrait.getVille());
			ResultSet resultatRetrait = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatRetrait.next()) {
				// Récupération de l'id du retrait résulté par la requête
				ID = resultatRetrait.getInt(1);
				resultatRetrait.close();
				pstmt.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ID;
	}

	@Override
	public Retrait selectRetraitByID(Integer idRetrait) {

		Retrait retrait = new Retrait();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_RETRAIT_BY_ID);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idRetrait);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {
				// Création d'un nouveau retrait à l'aide des données récupérées
				retrait = new Retrait(idRetrait, resultatUser.getString("rue"), resultatUser.getString("cp"),
						resultatUser.getString("ville"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return retrait;
	}

	@Override
	public Integer insertRetrait(Retrait retrait) {

		Integer ID = null;

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_RETRAIT, PreparedStatement.RETURN_GENERATED_KEYS);

			// Ajout des valeurs du retrait dans la requête
			pstmt.setString(1, retrait.getRue());
			pstmt.setString(2, retrait.getCp());
			pstmt.setString(3, retrait.getVille());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();

			// S'il y a un résultat ...
			if (rs.next()) {
				// Auto-incrémentation de l'id
				ID = rs.getInt(1);
				rs.close();
				pstmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return ID;
	}

	// Methodes pour `enchere`

	public void insertEnchere(Enchere enchere) {

		Integer ID = null;
		ZoneId defaultZoneId = ZoneId.systemDefault();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(INSERT_ENCHERE, PreparedStatement.RETURN_GENERATED_KEYS);

			// Ajout des valeurs de l'enchere dans la requête
			pstmt.setDate(1, new java.sql.Date(
					Date.from((enchere.getDate()).atStartOfDay(defaultZoneId).toInstant()).getTime()));
			pstmt.setDouble(2, enchere.getMontant());
			pstmt.setInt(3, enchere.getIdArticle());
			pstmt.setInt(4, enchere.getIdAcheteur());
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();

			// S'il y a un résultat ...
			if (rs.next()) {
				// Auto-incrémentation de l'id
				ID = rs.getInt(1);
				enchere.setId(ID);
				rs.close();
				pstmt.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Enchere selectLastEnchereByIdArticle(Integer idArticle) {

		Enchere enchere = new Enchere();

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(SELECT_ENCHERE_BY_ID_ARTICLE);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idArticle);
			ResultSet resultatUser = pstmt.executeQuery();

			// S'il y a un résultat ...
			if (resultatUser.next()) {
				// Création d'une nouvelle enchère avec les données récupérées
				enchere = new Enchere(resultatUser.getInt("id_enchere"),
						resultatUser.getDate("date_enchere").toLocalDate(), resultatUser.getDouble("montant_enchere"),
						resultatUser.getInt("id_article"), resultatUser.getInt("id_acheteur"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return enchere;
	}

	@Override
	public void deleteEnchereById(Integer idEnchere) {

		try (Connection cnx = ConnectionProvider.getConnection()) {
			PreparedStatement pstmt = cnx.prepareStatement(DELETE_ENCHERE_BY_ID);

			// Ajout de l'id spécifié dans la requête
			pstmt.setInt(1, idEnchere);
			pstmt.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}