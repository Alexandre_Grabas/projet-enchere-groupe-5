package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet("/Identification")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletLogin() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String identifiant = request.getParameter("identifiant");
		String password = request.getParameter("password");
		
		EnchereManager manager = new EnchereManager();

		resterConnecter(request, response, identifiant, password);

		verificationConnexion(request, response, identifiant, password, manager);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
		rd.forward(request, response);
	}

	/**
	 * Methode vérifiant si le pseudo/email correspond au mot de passe rentrer par l'utilisateur et renvoyant a l'accueil si 
	 * les information rentrez conviennent
	 * @param request HttpServletRequest request de la servet
	 * @param response HttpServletResponse response de la servet
	 * @param identifiant l'identifiant rentrez pas l'utilisateur
	 * @param password le mdp rentrer par l'utilisateur
	 * @param manager manager de la class EnchereManager de la bll
	 */
	private void verificationConnexion(HttpServletRequest request, HttpServletResponse response, String identifiant,
			String password, EnchereManager manager) {
		try {
			Utilisateur utilisateur = manager.connectionUtilisateur(identifiant, password);
			if (utilisateur != null) {
				HttpSession session = request.getSession();
				session.setAttribute("utilisateur",utilisateur);
				RequestDispatcher rd = request.getRequestDispatcher("/Accueil");
				rd.forward(request, response);
			} else {
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/login.jsp");
				rd.forward(request, response);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * methode créant 2 coockie d'une durée de vie de 30 jours permettant de se souvenir du login et du mdp de l'utilisateur si la case rester connecter est coché
	 * @param request HttpServletRequest request de la servet
	 * @param response HttpServletResponse response de la servet
	 * @param identifiant l'identifiant rentrez pas l'utilisateur
	 * @param password le mdp rentrer par l'utilisateur
	 */
	private void resterConnecter(HttpServletRequest request, HttpServletResponse response, String identifiant,
			String password) {
		if (request.getParameter("resterConnecter") != null) {
		    Cookie identifiantCookie = new Cookie("identifiant", identifiant);
		    Cookie passwordCookie = new Cookie("password", password);

		    //Durée de vie des cookies (ici 1 jours)
		    identifiantCookie.setMaxAge(1 * 24 * 60 * 60);
		    passwordCookie.setMaxAge(1 * 24 * 60 * 60);

		    response.addCookie(identifiantCookie);
		    response.addCookie(passwordCookie);
		}
	}
}
