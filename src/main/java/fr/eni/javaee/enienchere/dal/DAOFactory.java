/**
 * 
 */
package fr.eni.javaee.enienchere.dal;

/**
 * @author tlepriol2023
 *
 */
public class DAOFactory {
	public static EnchereDAO getEnchereDAO() {
		return new EnchereDAOjdbcImpl();
	}
}