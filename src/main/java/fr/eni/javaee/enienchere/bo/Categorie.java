package fr.eni.javaee.enienchere.bo;

public class Categorie {

	// Variables
	private Integer id;
	private String libelle;

	// Constructeurs

	public Categorie() {
	}

	public Categorie(Integer id, String libelle) {
		super();
		this.id = id;
		this.libelle = libelle;
	}

	// Getters

	public Integer getId() {
		return id;
	}

	public String getLibelle() {
		return libelle;
	}

	// Setters

	public void setId(Integer id) {
		this.id = id;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	// ToString

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Categorie [id=");
		builder.append(id);
		builder.append(", libelle=");
		builder.append(libelle);
		builder.append("]");
		return builder.toString();
	}

}
