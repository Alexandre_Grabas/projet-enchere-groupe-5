<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
</head>
<body>
	<%
	Article article = (Article) request.getAttribute("articleObject");
	Retrait retrait = (Retrait) request.getAttribute("retrait");
	List<Categorie> categories = (List<Categorie>) request.getAttribute("categories");
	List<String> error_messages = (List<String>) request.getAttribute("error_messages");

	if (session.getAttribute("utilisateur") != null) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
	%>
	<header class="global-nav">
		<a class="link-primary" href="Accueil">ENI-Enchères</a>
	</header>
	<br>
	<form action="CreateArticle" method="POST">
		<div class="formulaire container-fluid border border-dark col-6">
			<div class="col-12-sm">
				<label class="col-3-sm">Article :</label> <input class="col-9-sm"
					type="text" name="nom" value="<%=article.getNom()%>" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Description : </label>
				<textarea class="col-9-sm" name="description" rows="5" cols="33"
					required><%=article.getDescription()%></textarea>
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Catégories :</label> <select
					class="col-9-sm" name="categorie">
					<%
					for (int i = 0; i < categories.size(); i++) {
					%>
					<option value=<%=categories.get(i).getId()%>
						<%if (article.getIdCategorie() == categories.get(i).getId()) {%>
						selected <%}%>><%=categories.get(i).getLibelle()%></option>
					<%
					}
					%>
				</select>
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Mise à prix : </label> <input
					class="col-9-sm" type="number" name="mise" min="0" max="10000"
					placeholder="100" value="<%=article.getMiseAPrix()%>" required>
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Début de l'enchère : </label> <input
					class="col-9-sm" type="date" name="dateIn" placeholder="jj/mm/aaaa"
					value="<%=article.getDateDebutEncheres()%>" required>
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Fin de l'enchère : </label> <input
					class="col-9-sm" type="date" name="dateOut"
					placeholder="jj/mm/aaaa" value="<%=article.getDateFinEncheres()%>"
					required>
			</div>
			<br> <a>Retrait</a>
			<div class="container col-9-sm border border-1">
				<%
				if (article.getId() != 0) {
				%>
				<div class="col-12-sm">
					<label class="col-3-sm">Rue : </label> <input class="col-9-sm"
						type="text" name="rue" value="<%=retrait.getRue()%>" required />
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm">Code postal : </label> <input
						class="col-9-sm" type="text" name="cp" value=<%=retrait.getCp()%>
						required />
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm">Ville : </label> <input class="col-9-sm"
						type="text" name="ville" value=<%=retrait.getVille()%> required />
				</div>
				<%
				}
				%>
				<%
				if (article.getId() == 0) {
				%>
				<div class="col-12-sm">
					<label class="col-3-sm">Rue : </label> <input class="col-9-sm"
						type="text" name="rue" value="<%=utilisateur.getRue()%>" required />
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm">Code postal : </label> <input
						class="col-9-sm" type="text" name="cp"
						value=<%=utilisateur.getCp()%> required />
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm">Ville : </label> <input class="col-9-sm"
						type="text" name="ville" value=<%=utilisateur.getVille()%>
						required />
				</div>
				<%
				}
				%>
			</div>
			<br>
			<%
			if (error_messages != null) {
			%>
			<%
			for (int i = 0; i < error_messages.size(); i++) {
			%>
			<ul>
				<li><%=error_messages.get(i)%></li>
			</ul>
			<%
			}
			}
			%>
		</div>
		<br>
		<input type=hidden name=idArticle value=<%=article.getId()%>>
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Enregistrer</button>
	</form>
	<form action=Accueil method="GET">
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Annuler</button>
	</form>
	<%
	if (article.getId() != 0) {
	%>
	<form action=DeleteArticle method="POST">
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Annuler la vente</button>
		<input type=hidden name=idArticle value=<%=article.getId()%>>
	</form>
	<%
	}
	}
	if (session.getAttribute("utilisateur") == null) {
	%>
	<h2>Utilisateur non récupéré ou session expirée</h2>
	<%
	}
	%>
</body>
</html>