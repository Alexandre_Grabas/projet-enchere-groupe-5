<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
</head>
<body>
	<%
	Utilisateur user = (Utilisateur) request.getSession().getAttribute("utilisateur");
	String identifiant = "";
	String password = "";

	Cookie[] cookies = request.getCookies();
	if (cookies != null) {
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("identifiant")) {
		identifiant = cookie.getValue();
			} else if (cookie.getName().equals("password")) {
		password = cookie.getValue();
			}
		}
	}
	%>
	<header class="global-nav">
		<a class="link-primary" href="Accueil">ENI-Enchères</a>
	</header>
	<br>
	<%
	if (user != null) {
	%>
	<label>Vous étez déjà connectée</label>
	<%
	} else {
	%>
	<h2 class="text-center">Connexion</h2>
	<br>
	<div class="formulaire container-fluid border border-dark col-3">
		<form method="post" action="Identification">
			<div class="col-12-sm">
				<div class="col-12-sm">
					<label class="col-3-sm" for="input_identifiant">Identifiant
						:</label> <input id="input_identifiant" type="text"
						value="<%=identifiant%>" name="identifiant" />
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm" for="input_password">Mot de passe :</label>
					<input id="input_password" type="password" value="<%=password%>"
						name="password" />
				</div>
				<div class="col-2-sm">
					<button class="container-fluid btn btn-primary" type="submit"
						value="login.jsp" name="Page">Connexion</button>
				</div>
				<div class="col-12-sm">
					<label class="col-3-sm">Se souvenir de moi</label> <input
						type="checkbox" name="resterConnecter" value="oui">
				</div>
				<a href="<%=request.getContextPath()%>/Identification" hidden>Mot de
					passe oublié</a>
			</div>
		</form>
	</div>
	<br>
	<br>
	<div class="container-fluid col-3">
		<form method="GET" action="AddUser">
			<div class="col-4-sm">
				<button class="container-fluid btn btn-primary" type="submit">Créer
					un compte</button>
			</div>
		</form>
	</div>
	<%
	}
	%>
</body>
</html>