<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
<body>
	<header class="global-nav">
		<a class="link-primary" href="Accueil">ENI-Enchères</a>
	</header>
	<br>
	<form action="AddUser" method="POST">
		<div class="formulaire container-fluid border border-dark col-6">
			<div class="col-12-sm">
				<label class="col-3-sm">Pseudo : </label> <input class="col-9-sm" type="text"
					name="pseudo" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Nom : </label> <input class="col-9-sm" type="text"
					name="nom" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Prénom : </label> <input class="col-9-sm" type="text"
					name="prenom" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Email : </label> <input class="col-9-sm" type="text"
					name="email" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Téléphone : </label> <input class="col-9-sm" type="text"
					name="telephone" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Rue : </label> <input class="col-9-sm" type="text"
					name="rue" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Code Postal : </label> <input class="col-9-sm" type="text"
					name="cp" required />
			</div>
			<div class="col-12-sm">
				<label class="text-list">Ville : </label> <input class="col-9-sm" type="text"
					name="ville" required />
			</div>
			<div class="col-12-sm">
				<label class="text-list">Mot de passe : </label> <input class="col-9-sm"
					type="password" name="password" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Confirmation : </label> <input class="col-9-sm"
					type="password" name="confirm_password" required />
			</div>
		</div>
		<br>
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Créer</button>
	</form>

	<form action="Accueil" method="GET">
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Annuler</button>
	</form>

</body>
</html>