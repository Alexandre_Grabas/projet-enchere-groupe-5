<%@page import="fr.eni.javaee.enienchere.bo.*"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
</head>
<body>
	<%
	List<String> error_messages = (List<String>) request.getAttribute("error_messages");
	%>
	<%
	if (session.getAttribute("utilisateur") != null) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
	%>

	<header class="global-nav">
		<a class="link-primary" href="Accueil">ENI-Enchères</a>
	</header>
	<br>
	<form action="ModifieUser" method="POST">
		<div class="formulaire container-fluid border border-dark col-6">
			<div class="col-12-sm">
				<label class="col-3-sm">Pseudo : </label> <input type="text" name="pseudo"
					value="<%=utilisateur.getPseudo()%>" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Nom : </label> <input type="text" name="nom"
					value="<%=utilisateur.getNom()%>" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Prénom : </label> <input type="text" name="prenom"
					value="<%=utilisateur.getPrenom()%>" required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Email : </label> <input type="text" name="email"
					value=<%=utilisateur.getEmail()%> required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Téléphone : </label> <input type="text"
					name="telephone" value=<%=utilisateur.getTelephone()%> required />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Rue : </label> <input type="text" name="rue"
					value="<%=utilisateur.getRue()%>" />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Code Postal : </label> <input type="text" name="cp"
					value=<%=utilisateur.getCp()%> />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Ville : </label> <input type="text" name="ville"
					value=<%=utilisateur.getVille()%> />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Mot de passe actuel : </label> <input
					type="password" name="pwd" value=<%=utilisateur.getMdp()%> />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Nouveau mot de passe : </label> <input
					type="password" name="new_pwd" name="pseudo" value="" />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Confirmation : </label> <input type="password"
					name="confirm_pwd" value="" />
			</div>
			<div class="col-12-sm">
				<label class="col-3-sm">Credit : </label> <input type="text"
					value=<%=utilisateur.getCredit()%> disabled="disabled" />
			</div>
			<br>
			<%
			if (error_messages != null) {
			%>
			<%
			for (int i = 0; i < error_messages.size(); i++) {
			%>
			<ul>
				<li><%=error_messages.get(i)%></li>
			</ul>
			<%
			}
			}
			%>
		</div>
		<br>
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary">Enregistrer</button>
	</form>

	<form action="DeleteUser" method="POST">
		<button type="submit" class="container-fluid col-md-4 offset-md-4 btn btn-primary" >supprimer</button>
	</form>
	<%
	}
	if (session.getAttribute("utilisateur") == null) {
	%>
	<h2>Utilisateur non récupéré ou session expirée</h2>
	<%
	}
	%>

</body>
</html>