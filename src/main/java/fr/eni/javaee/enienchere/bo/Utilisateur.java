package fr.eni.javaee.enienchere.bo;

import java.io.Serializable;

public class Utilisateur implements Serializable {

	private static final long serialVersionUID = 1L;

	// Variables
	private Integer id;
	private String pseudo;
	private String nom;
	private String prenom;
	private String email;
	private String telephone;
	private String rue;
	private String ville;
	private String cp;
	private String mdp;
	private Integer credit = 1000;
	private boolean administrateur = false;

	// Constructeurs
	public Utilisateur() {
	}

	public Utilisateur(String pseudo, String mdp) {
		this.pseudo = pseudo;
		this.mdp = mdp;
	}

	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp) {
		super();
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
	}

	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp, Integer credit) {
		super();
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
		this.credit = credit;
	}

	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp, boolean administrateur) {
		super();
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
		this.administrateur = administrateur;
	}

	public Utilisateur(String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp, Integer credit, boolean administrateur) {
		super();
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
		this.credit = credit;
		this.administrateur = administrateur;
	}

	public Utilisateur(Integer id, String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp, Integer credit, boolean administrateur) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
		this.credit = credit;
		this.administrateur = administrateur;
	}

	public Utilisateur(Integer id, String pseudo, String nom, String prenom, String email, String telephone, String rue,
			String ville, String cp, String mdp) {
		super();
		this.id = id;
		this.pseudo = pseudo;
		this.nom = nom;
		this.prenom = prenom;
		this.email = email;
		this.telephone = telephone;
		this.rue = rue;
		this.ville = ville;
		this.cp = cp;
		this.mdp = mdp;
	}

	// Getters

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getEmail() {
		return email;
	}

	public String getTelephone() {
		return telephone;
	}

	public String getRue() {
		return rue;
	}

	public String getVille() {
		return ville;
	}

	public String getCp() {
		return cp;
	}

	public String getMdp() {
		return mdp;
	}

	public Integer getCredit() {
		return credit;
	}

	public boolean isAdministrateur() {
		return administrateur;
	}

	// Setters

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public void setCredit(Integer credit) {
		this.credit = credit;
	}

	public void setAdministrateur(boolean administrateur) {
		this.administrateur = administrateur;
	}

	// ToString

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Utilisateur [id=");
		builder.append(id);
		builder.append(", pseudo=");
		builder.append(pseudo);
		builder.append(", nom=");
		builder.append(nom);
		builder.append(", prenom=");
		builder.append(prenom);
		builder.append(", email=");
		builder.append(email);
		builder.append(", telephone=");
		builder.append(telephone);
		builder.append(", rue=");
		builder.append(rue);
		builder.append(", ville=");
		builder.append(ville);
		builder.append(", cp=");
		builder.append(cp);
		builder.append(", mdp=");
		builder.append(mdp);
		builder.append(", credit=");
		builder.append(credit);
		builder.append(", administrateur=");
		builder.append(administrateur);
		builder.append("]");
		return builder.toString();
	}

}
