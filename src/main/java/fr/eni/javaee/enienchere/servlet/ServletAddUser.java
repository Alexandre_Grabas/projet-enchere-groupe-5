package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class ServletCreateUser
 */
@WebServlet("/AddUser")
public class ServletAddUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletAddUser() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/addUser.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Utilisateur utilisateur = new Utilisateur();
		request.setCharacterEncoding("UTF-8");
		EnchereManager enchereManager = new EnchereManager();

		utilisateur.setPseudo(request.getParameter("pseudo"));
		utilisateur.setNom(request.getParameter("nom"));
		utilisateur.setPrenom(request.getParameter("prenom"));
		utilisateur.setEmail(request.getParameter("email"));
		utilisateur.setTelephone(request.getParameter("telephone"));
		utilisateur.setRue(request.getParameter("rue"));
		utilisateur.setCp(request.getParameter("cp"));
		utilisateur.setVille(request.getParameter("ville"));
		if (request.getParameter("password").equals(request.getParameter("confirm_password"))) {
			utilisateur.setMdp(request.getParameter("password"));
			try {
				enchereManager.ajouterUtilisateur(utilisateur);
				RequestDispatcher rd = request.getRequestDispatcher("/Accueil");
				rd.forward(request, response);
			} catch (EnchereException e) {
				System.out.println(e.getMessage());
				RequestDispatcher rd = request.getRequestDispatcher("/AddUser");
				rd.forward(request, response);
			}
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/AddUser");
			rd.forward(request, response);
		}
	}

}
