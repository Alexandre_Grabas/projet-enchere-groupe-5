<%@page import="fr.eni.javaee.enienchere.bo.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ"
	crossorigin="anonymous">
<link rel="stylesheet" href="./css/eniEnchereV2.css">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
	crossorigin="anonymous"></script>
<title>EniEnchere</title>
</head>
<body>
	<%
	Utilisateur other_user = (Utilisateur)request.getAttribute("other_user");
	if (session.getAttribute("utilisateur") != null) {
		Utilisateur utilisateur = (Utilisateur) session.getAttribute("utilisateur");
		if (other_user != null) {
			utilisateur = other_user;
		}
	%>

	<header class="global-nav">
		<a class="link-primary" href="Accueil">ENI-Enchères</a>
	</header>
	<br>
	<div class="formulaire container-fluid border border-dark col-6">
		<div class="col-12-sm">
			<label class="text-list">Pseudo : </label> <input type="text"
				value=<%=utilisateur.getPseudo()%> disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Nom : </label> <input
				type="text" value="<%=utilisateur.getNom()%>" disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Prénom : </label> <input
				type="text" value=<%=utilisateur.getPrenom()%> disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Email : </label> <input
				type="text" value=<%=utilisateur.getEmail()%> disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Téléphone : </label> <input
				type="text" value=<%=utilisateur.getTelephone()%>
				disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="text-list">Rue : </label> <input type="text"
				value="<%=utilisateur.getRue()%>" disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Ville : </label> <input
				type="text" value=<%=utilisateur.getVille()%> disabled="disabled" />
		</div>
		<div class="col-12-sm">
			<label class="col-3-sm" class="text-list">Code Postal : </label> <input
				type="text" value=<%=utilisateur.getCp()%> disabled="disabled" />
		</div>
		<br>
	</div>
	<br>
	<% if (other_user == null) {%>
	<form action="ModifieUser" method="GET">
		<button class="container-fluid col-md-4 offset-md-4 btn btn-primary" type="submit">Modifier</button>
	</form>
	<%} %>
	<%
	}
	if (session.getAttribute("utilisateur") == null) {
	%>
	<h2>Utilisateur non récupéré ou session expirée</h2>
	<%
	}
	%>

</body>
</html>