package fr.eni.javaee.enienchere.bll;

import java.util.List;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Categorie;
import fr.eni.javaee.enienchere.bo.Enchere;
import fr.eni.javaee.enienchere.bo.Retrait;
import fr.eni.javaee.enienchere.bo.Utilisateur;
import fr.eni.javaee.enienchere.dal.DAOFactory;
import fr.eni.javaee.enienchere.dal.EnchereDAO;

public class EnchereManager {

	private EnchereDAO enchereDao;

	public EnchereManager() {
		this.enchereDao = DAOFactory.getEnchereDAO();
	}

	public Utilisateur connectionUtilisateur(String identifiant, String mdp) throws EnchereException {

		EnchereException enchereException = new EnchereException();
		this.validerPseudo(identifiant, enchereException);
		this.validerMdp(mdp, enchereException);

		Utilisateur utilisateur = new Utilisateur();

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			utilisateur = this.enchereDao.selectUserByEmailOrPseudo(identifiant, mdp);
		} else {
			throw enchereException;
		}

		return utilisateur;

	}

	public void ajouterUtilisateur(Utilisateur utilisateur) throws EnchereException {

		EnchereException enchereException = new EnchereException();
		validerUtilisateur(utilisateur, enchereException);

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			this.enchereDao.insertUser(utilisateur);
		} else {
			throw enchereException;
		}

	}

	public Integer ajouterRetrait(Retrait retrait) throws EnchereException {

		EnchereException enchereException = new EnchereException();
		validerRue(retrait.getRue(), enchereException);
		validerCp(retrait.getCp(), enchereException);
		validerVille(retrait.getVille(), enchereException);
		Integer ID = null;

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			ID = this.enchereDao.insertRetrait(retrait);
		} else {
			throw enchereException;
		}

		return ID;
	}

	public Integer ajouterArticle(Article a) throws EnchereException {

		EnchereException enchereException = new EnchereException();
		validerRue(a.getDescription(), enchereException);
		validerRue(a.getNom(), enchereException);
		Integer ID = null;

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			ID = this.enchereDao.insertArticle(a);
		} else {
			throw enchereException;
		}

		return ID;
	}

	public void supprimeArticle(Integer idArticle) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		validerId(idArticle, enchereException);

		if (!enchereException.hasErreurs()) {
			this.enchereDao.deleteArticleById(idArticle);
		} else {
			throw enchereException;
		}
	}

	public void supprimerUtilisateur(Integer idUtilisateur) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		validerId(idUtilisateur, enchereException);

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			this.enchereDao.deleteUserById(idUtilisateur);
		} else {
			throw enchereException;
		}
	}

	public void updateUtilisateurById(Utilisateur utilisateur) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		validerUtilisateur(utilisateur, enchereException);

		if (!enchereException.hasErreurs()) {
			// TODO : ajout de Try et Catch quand la methode selectUserByEmailOrPseudo
			// pourra renvoyer des exceptions
			this.enchereDao.updateUserById(utilisateur);
		} else {
			throw enchereException;
		}
	}

	public void updateCreditUtilisateurById(Integer idUtilisateur, Integer Credit) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		validerId(idUtilisateur, enchereException);

		if (!enchereException.hasErreurs()) {
			this.enchereDao.updateCreditUserById(idUtilisateur, Credit);
		} else {
			throw enchereException;
		}
	}

	public Utilisateur selectUtilisateurById(Integer idUtilisateur) {
		return this.enchereDao.selectUserById(idUtilisateur);
	}

	public List<Article> selectFiltredArticle(String filtre, String categorie) {
		return this.enchereDao.selectArticleFiltrer(filtre, categorie);

	}

	public List<Article> selectArticleFiltrerConected(Integer idUtilisateur, String filtreNom, String categorie) {
		return this.enchereDao.selectArticleFiltrerConected(idUtilisateur, filtreNom, categorie);

	}

	public List<Article> selectArticleEnchereByUserFiltrer(Integer idUtilisateur, String filtreNom, String categorie) {
		return this.enchereDao.selectArticleEnchereByUserFiltrer(idUtilisateur, filtreNom, categorie);

	}

	public List<Article> selectArticleEnchereByUserFiltrerWin(Integer idUtilisateur, String filtreNom,
			String categorie) {
		return this.enchereDao.selectArticleEnchereByUserFiltrerWin(idUtilisateur, filtreNom, categorie);

	}

	public List<Article> selectArticleEnchereByUserSelingInProgress(Integer idUtilisateur, String filtreNom,
			String categorie) {
		return this.enchereDao.selectArticleEnchereByUserSelingInProgress(idUtilisateur, filtreNom, categorie);
	}

	public List<Article> selectArticleEnchereByUserSelingNotStarted(Integer idUtilisateur, String filtreNom,
			String categorie) {
		return this.enchereDao.selectArticleEnchereByUserSelingNotStarted(idUtilisateur, filtreNom, categorie);
	}

	public List<Article> selectArticleEnchereByUserSelingEnded(Integer idUtilisateur, String filtreNom,
			String categorie) {
		return this.enchereDao.selectArticleEnchereByUserSelingEnded(idUtilisateur, filtreNom, categorie);
	}

	public List<Article> selectAllArticle() {
		return this.enchereDao.selectAllArticle();
	}

	public Article selectArticleById(Integer idArticle) {
		return this.enchereDao.selectArticleById(idArticle);
	}

	public void updateArticle(Article article) {
		this.enchereDao.updateArticle(article);
	}

	public void updatePrixArticleById(Integer idArticle, Double montant) {
		this.enchereDao.updatePrixArticleById(idArticle, montant);
	}
	
	public void updateEtatArticleById(Integer idArticle, Integer etatVente) throws EnchereException{
		EnchereException enchereException = new EnchereException();
		validerId(idArticle, enchereException);
		validerEtat(etatVente, enchereException);

		if (!enchereException.hasErreurs()) {
			this.enchereDao.updateEtatArticleById(idArticle, etatVente);
		} else {
			throw enchereException;
		}
	}

	public List<Categorie> selectAllCategories() {
		return this.enchereDao.selectAllCategories();
	}

	public Categorie selectCategorieById(Integer idCategorie) {
		return this.enchereDao.selectCategorieByID(idCategorie);
	}

	public Retrait selectRetraitById(Integer idRetrait) {
		return this.enchereDao.selectRetraitByID(idRetrait);
	}

	public Integer selectRetraitIfPresent(Retrait retrait) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		Integer result = null;
		validerRue(retrait.getRue(), enchereException);
		validerCp(retrait.getCp(), enchereException);
		validerVille(retrait.getVille(), enchereException);

		if (!enchereException.hasErreurs()) {
			result = this.enchereDao.selectSpecificRetrait(retrait);
		} else {
			throw enchereException;
		}
		return result;
	}

	public Enchere selectLastEnchereByIdArticle(Integer idArticle) {
		return this.enchereDao.selectLastEnchereByIdArticle(idArticle);
	}

	public void ajouterEnchere(Enchere enchere) throws EnchereException {

		EnchereException enchereException = new EnchereException();
		// validerEnchere(enchere, enchereException);

		if (!enchereException.hasErreurs()) {
			this.enchereDao.insertEnchere(enchere);
		} else {
			throw enchereException;
		}
	}

	public void deleteEnchereById(Integer idEnchere) throws EnchereException {
		EnchereException enchereException = new EnchereException();
		validerId(idEnchere, enchereException);
		if (!enchereException.hasErreurs()) {
			this.enchereDao.deleteEnchereById(idEnchere);
		} else {
			throw enchereException;
		}
	}

	private void validerId(Integer id, EnchereException enchereException) {
		// Vérifie qui l'id n'est pas "null"
		if (id == null || id == 0) {
			enchereException.ajouterErreur(20000);
		}
	}

	private void validerPseudo(String pseudo, EnchereException enchereException) {
		// Vérifie que le pseudo n'est ni "null" ni vide
		if (pseudo == null || pseudo.isBlank()) {
			enchereException.ajouterErreur(20001);
		}
	}

	private void validerNom(String nom, EnchereException enchereException) {
		// Vérifie que le nom n'est ni "null" ni vide
		if (nom == null || nom.isBlank()) {
			enchereException.ajouterErreur(20002);
		}
	}

	private void validerPrenom(String prenom, EnchereException enchereException) {
		// Vérifie que le prenom n'est ni "null" ni vide
		if (prenom == null || prenom.isBlank()) {
			enchereException.ajouterErreur(20003);
		}
	}

	private void validerEmail(String email, EnchereException enchereException) {
		// Vérifie que l'email n'est ni "null" ni vide
		if (email == null || email.isBlank()) {
			enchereException.ajouterErreur(20004);
		}
	}

	private void validerTelephone(String tel, EnchereException enchereException) {
		// Vérifie que le téléphone n'est ni "null" ni vide
		if (tel == null || tel.isBlank()) {
			enchereException.ajouterErreur(20005);
		}
	}

	private void validerVille(String ville, EnchereException enchereException) {
		// Vérifie que la ville n'est ni "null" ni vide
		if (ville == null || ville.isBlank()) {
			enchereException.ajouterErreur(20006);
		}
	}

	private void validerCp(String cp, EnchereException enchereException) {
		// Vérifie que le code postal n'est ni "null" ni vide
		if (cp == null || cp.isBlank()) {
			enchereException.ajouterErreur(20007);
		}
	}

	private void validerRue(String rue, EnchereException enchereException) {
		// Vérifie que le code postal n'est ni "null" ni vide
		if (rue == null || rue.isBlank()) {
			enchereException.ajouterErreur(20008);
		}
	}

	private void validerMdp(String mdp, EnchereException enchereException) {
		// Vérifie que le mot de passe n'est ni "null" ni vide
		if (mdp == null || mdp.isBlank()) {
			enchereException.ajouterErreur(20009);
		}
	}

	private void validerUtilisateur(Utilisateur utilisateur, EnchereException enchereException) {
		// Verifie que les variables de l'utilisateur ne sont ni "null" ni vide
		validerPseudo(utilisateur.getPseudo(), enchereException);
		validerNom(utilisateur.getNom(), enchereException);
		validerPrenom(utilisateur.getPrenom(), enchereException);
		validerEmail(utilisateur.getEmail(), enchereException);
		validerTelephone(utilisateur.getTelephone(), enchereException);
		validerRue(utilisateur.getRue(), enchereException);
		validerCp(utilisateur.getCp(), enchereException);
		validerVille(utilisateur.getVille(), enchereException);
		validerMdp(utilisateur.getMdp(), enchereException);
	}
	

	private void validerEtat(Integer etatVente, EnchereException enchereException) {
		// Verifie que l'etat de vente n'est pas "null"
		if (etatVente == null) {
			enchereException.ajouterErreur(30000);
		} else {
			// Verifie que la valeur de l'etat de vente est 0 ou 1 pour correspondre à un boolean
			if (etatVente != 0 || etatVente == 1) {
				enchereException.ajouterErreur(30001);
			}
		}
	}

}
