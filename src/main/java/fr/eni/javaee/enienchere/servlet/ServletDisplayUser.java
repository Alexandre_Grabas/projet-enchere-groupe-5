package fr.eni.javaee.enienchere.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.bll.EnchereManager;

/**
 * Servlet implementation class ServletAfficherProfil
 */
@WebServlet("/DisplayUser")
public class ServletDisplayUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ServletDisplayUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String id = request.getParameter("ID");
		
		// Si on nous renvoie un ID, on affiche l'userID sinon on affiche l'utilisateur en cours de session
		if (id != null) {
			EnchereManager newuser = new EnchereManager();
			request.setAttribute("other_user",newuser.selectUtilisateurById(Integer.parseInt(id)));
		}
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/displayUser.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
}