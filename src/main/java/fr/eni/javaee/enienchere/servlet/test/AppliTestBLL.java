package fr.eni.javaee.enienchere.servlet.test;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.EnchereException;
import fr.eni.javaee.enienchere.bll.EnchereManager;
import fr.eni.javaee.enienchere.bo.Utilisateur;

/**
 * Servlet implementation class AppliTestBLL
 */
@WebServlet("/AppliTestBLL")
public class AppliTestBLL extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AppliTestBLL() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Création d'un utilisateur
		Utilisateur utilisateur = new Utilisateur("Gpadiday", "Padiday", "Gerard", "gpadiday@gmail.com", "0425365869",
				"rue des oliviers", "Rennes", "35000", "1234");

		// Insertion d'un utilisateur dans la BD

		EnchereManager enchereManager = new EnchereManager();

		try {
			enchereManager.ajouterUtilisateur(utilisateur);
		} catch (EnchereException e) {
			System.out.println(e.getMessage());
		}

		// Récupération de l'id d'un utilisateur

		String pseudo = utilisateur.getPseudo();
		String mdp = utilisateur.getMdp();

		try {
			utilisateur = enchereManager.connectionUtilisateur(pseudo, mdp);
		} catch (EnchereException e) {
			System.out.println(e.getMessage());
		}

		response.getWriter().append("Insertion d'un utilisateur d'id ")
				.append(utilisateur != null ? utilisateur.getId() + "" : "null");

		// Modification d'un utilisateur dans la BD

		response.getWriter().append("\nUtilisateur avant modification : ").append(utilisateur + "");

		utilisateur.setRue("Avenue de la fin des cours");
		utilisateur.setCp("29000");
		utilisateur.setVille("Brest");

		try {
			enchereManager.updateUtilisateurById(utilisateur);
		} catch (EnchereException e) {
			System.out.println(e.getMessage());
		}

		try {
			utilisateur = enchereManager.connectionUtilisateur(pseudo, mdp);
		} catch (EnchereException e) {
			System.out.println(e.getMessage());
		}

		response.getWriter().append("\nUtilisateur après modification : ").append(utilisateur + "");

		// Suppression de l'utilisateur dans la BD

		try {
			enchereManager.supprimerUtilisateur(utilisateur.getId());
		} catch (EnchereException e) {
			System.out.println(e.getMessage());
		}

		response.getWriter().append("\nSuppression de l'utilisateur d'id ")
				.append(utilisateur != null ? utilisateur.getId() + "" : "null");

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
