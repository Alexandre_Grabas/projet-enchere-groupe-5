package fr.eni.javaee.enienchere.dal;

import java.util.List;

import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Categorie;
import fr.eni.javaee.enienchere.bo.Enchere;
import fr.eni.javaee.enienchere.bo.Retrait;
import fr.eni.javaee.enienchere.bo.Utilisateur;

public interface EnchereDAO {

	// Methodes pour `utilisateur`

	public Utilisateur selectUserByEmailOrPseudo(String identifiant, String pwd);

	public Utilisateur selectUserById(Integer idUtilisateur);

	public Integer insertUser(Utilisateur utilisateur);

	public void updateUserById(Utilisateur utilisateur);

	public void updateCreditUserById(Integer idUtilisateur, Integer credit);

	public void deleteUserById(Integer idUtilisateur);

	// Methodes pour `article`

	public List<Article> selectAllArticle();

	public Article selectArticleById(Integer idArticle);

	public List<Article> selectArticleFiltrer(String filtreNom, String categorie);

	public List<Article> selectArticleFiltrerConected(Integer idUtilisateur, String filtreNom, String categorie);

	public List<Article> selectArticleEnchereByUserFiltrer(Integer idUtilisateur, String filtreNom, String categorie);

	public List<Article> selectArticleEnchereByUserFiltrerWin(Integer idUtilisateur, String filtreNom,
			String categorie);

	public List<Article> selectArticleEnchereByUserSelingInProgress(Integer idUtilisateur, String filtreNom,
			String categorie);

	public List<Article> selectArticleEnchereByUserSelingNotStarted(Integer idUtilisateur, String filtreNom,
			String categorie);

	public List<Article> selectArticleEnchereByUserSelingEnded(Integer idUtilisateur, String filtreNom,
			String categorie);

	public Integer insertArticle(Article article);

	public void updateArticle(Article article);

	public void updatePrixArticleById(Integer idArticle, Double montant);

	public void updateEtatArticleById(Integer idArticle, Integer etatVente);

	public void deleteArticleById(Integer idArticle);

	// Methodes pour `categorie`

	public List<Categorie> selectAllCategories();

	public Categorie selectCategorieByID(Integer idCategorie);

	// Methodes pour `retrait`

	public Integer selectSpecificRetrait(Retrait retrait);

	public Retrait selectRetraitByID(Integer idRetrait);

	public Integer insertRetrait(Retrait retrait);

	// Methodes pour `enchere`

	public Enchere selectLastEnchereByIdArticle(Integer idArticle);

	public void insertEnchere(Enchere enchere);

	public void deleteEnchereById(Integer idEnchere);

}
