package fr.eni.javaee.enienchere.servlet.test;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.javaee.enienchere.bo.Article;
import fr.eni.javaee.enienchere.bo.Utilisateur;
import fr.eni.javaee.enienchere.dal.EnchereDAOjdbcImpl;

/**
 * Servlet implementation class AplpiTestJdbcImpl
 */
@WebServlet("/AppliTestJdbcImpl")
public class AppliTestJdbcImpl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AppliTestJdbcImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Utilisateur u = new Utilisateur("TOTO", "Le Priol", "Thomas", "toto@email.com", "0666666666", "rue du mene",
				"vannes", "56000", "123456", true);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate localDate = LocalDate.parse("2023-11-18", dateTimeFormatter);
		Article a = new Article("Guitare", "description", LocalDate.now(), localDate, 45.50, 55.00, 2, 4, 3);
		EnchereDAOjdbcImpl test = new EnchereDAOjdbcImpl();

//		Test pour l'insertion d'un utilisateur
		test.insertUser(u);

//		Test pour la s�lection d'un utilisateur
		Utilisateur u2 = test.selectUserByEmailOrPseudo("TOTO", "123456");
		System.out.println("u2: " + u2);
		Utilisateur u3 = test.selectUserByEmailOrPseudo("toto@email.com", "123456");
		System.out.println("u3: " + u3);
		Utilisateur u4 = test.selectUserByEmailOrPseudo("TITI", "123456789");
		System.out.println("u4: " + u4);

//		Test pour la suppression d'un utilisateur
//		Utilisateur u5 = new Utilisateur("TOTO", "Le Priol", "Thomas", "toto123@email.com", "0666666666", "rue du mene","vannes", "56000", "123456", true);
//		test.insertUser(u5);
//		test.deleteUserById(1);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
